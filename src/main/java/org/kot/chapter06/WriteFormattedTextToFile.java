package org.kot.chapter06;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.Random;

public class WriteFormattedTextToFile {
    public static void writeFormattedTextToFile() throws IOException {
        Random rnd = new Random();
        int[] intValues = new int[10];
        double[] doubleValues = new double[10];

        Arrays.setAll(intValues, (t) -> rnd.nextInt(100_000));
        Arrays.setAll(doubleValues, (t) -> rnd.nextDouble());

        Path path = Paths.get("numbersWithFormat.txt");

        try (BufferedWriter bw = Files.newBufferedWriter(path, StandardCharsets.UTF_8, StandardOpenOption.CREATE,
                StandardOpenOption.WRITE)) {
            for (int i = 0; i < 10; i++) {
                bw.write(String.format("| %6s | %7.3f |", intValues[i], doubleValues[i]));
                bw.newLine();
            }
        }
    }

    public static void main(String[] args) throws IOException {
        writeFormattedTextToFile();
    }
}
