package org.kot.chapter06;

import lombok.Data;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.util.EnumSet;

import static java.nio.file.StandardCopyOption.COPY_ATTRIBUTES;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

@Data
public class CopyFileVisitor implements FileVisitor {
    private final Path copyFrom;
    private final Path copyTo;

    public static void copySubTree(Path from, Path to) throws IOException {
        Files.copy(from, to, REPLACE_EXISTING, COPY_ATTRIBUTES);
    }

    @Override
    public FileVisitResult preVisitDirectory(Object dir, BasicFileAttributes attrs) throws IOException {
        Path newDir = copyTo.resolve(copyFrom.relativize((Path) dir));

        try {
            Files.copy((Path) dir, newDir, REPLACE_EXISTING, COPY_ATTRIBUTES);
        } catch (IOException ioe) {
            System.err.println("Failed to create " + newDir + " " + ioe.getMessage());

            return FileVisitResult.SKIP_SUBTREE;
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Object file, BasicFileAttributes attrs) throws IOException {

        try {
            copySubTree((Path) file, copyTo.resolve(copyFrom.relativize((Path) file)));
        } catch (IOException e) {
            System.err.println("Failed to copy " + copyFrom + " " + e.getMessage());
        }

        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFileFailed(Object file, IOException exc) throws IOException {
        if (exc instanceof FileSystemLoopException) {
            System.err.println("Loop detected " + file);
        } else {
            System.err.println("Failed to copy " + file + " " + exc.getMessage());
        }
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Object dir, IOException exc) throws IOException {
        Path newDir = copyTo.resolve(copyFrom.relativize((Path) dir));

        try {
            FileTime time = Files.getLastModifiedTime((Path) dir);
            Files.setLastModifiedTime(newDir, time);
        } catch (IOException e) {
            System.err.println("Failed to save time attribute in " + newDir + " " + e.getMessage());
        }
        return FileVisitResult.CONTINUE;
    }

    public static void main(String[] args) throws IOException {

        Path copyFrom = Paths.get("Z:/learning/packt");
        Path copyTo = Paths.get("Z:/е-courses");
        CopyFileVisitor copyFileVisitor = new CopyFileVisitor(copyFrom, copyTo);
        var opts = EnumSet.of(FileVisitOption.FOLLOW_LINKS);
        Files.walkFileTree(copyFrom, opts, Integer.MAX_VALUE, copyFileVisitor);
    }
}
