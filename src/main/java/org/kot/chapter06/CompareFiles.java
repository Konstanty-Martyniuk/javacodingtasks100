package org.kot.chapter06;

import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.EnumSet;

public class CompareFiles {
    private static final int MAP_SIZE = 5242880;

    public static boolean haveMismatches(Path path1, Path path2) throws IOException {
        try (FileChannel fileChannel1 = FileChannel.open(path1, EnumSet.of(StandardOpenOption.READ))){
            try (FileChannel fileChannel2 = FileChannel.open(path2, EnumSet.of(StandardOpenOption.READ))){
                long length1 = fileChannel1.size();
                long length2 = fileChannel2.size();

                if (length1 != length2) return true;

                int position = 0;
                while (position < length1) {
                    long remaining = length1 - position;
                    int bytestomap = (int) Math.min(MAP_SIZE, remaining);

                    MappedByteBuffer mBuffer1 = fileChannel1.map(FileChannel.MapMode.READ_ONLY, position, bytestomap);
                    MappedByteBuffer mBuffer2 = fileChannel2.map(FileChannel.MapMode.READ_ONLY, position, bytestomap);

                    while (mBuffer1.hasRemaining()) {
                        if (mBuffer1.get() != mBuffer2.get()) {
                            return true;
                        }
                    }
                    position += bytestomap;
                }
            }
        }
        return false;
    }

    public static void main(String[] args) throws IOException {
        Path file1 = Paths.get("file1.txt");
        Path file2 = Paths.get("file2.txt");
        //From jdk12+
        long mismatch12 = Files.mismatch(file1, file2);
    }
}
