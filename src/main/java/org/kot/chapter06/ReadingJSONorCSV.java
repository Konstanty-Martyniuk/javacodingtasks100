package org.kot.chapter06;


import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import com.fasterxml.jackson.core.*;
import org.kot.chapter05.Melon;
import org.kot.chapter05.MergeMap;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.*;
import java.util.regex.Pattern;

public class ReadingJSONorCSV {

    public static void main(String[] args) throws IOException {
        //==========================================JSON-B=============================================
        Jsonb jsonb = JsonbBuilder.create();
        Path pathArray = Paths.get("melons_array.json");
        Path pathMap = Paths.get("melons_map.json");
        Path pathRaw = Paths.get("melons_raw.json");

        //read melons_array as Array
        Melon[] melonsArray = jsonb.fromJson(Files.newBufferedReader(pathArray, StandardCharsets.UTF_8), Melon[].class);

        //read melons_array as list
        List<Melon> melonList = jsonb.fromJson(Files.newBufferedReader(pathArray, StandardCharsets.UTF_8), ArrayList.class);

        //read melons_map as map
        Map<String, Melon> melonMap = jsonb.fromJson(Files.newBufferedReader(pathMap, StandardCharsets.UTF_8), HashMap.class);

        //read json line by line into map
        Map<String, Melon> stringMap = new HashMap<>();
        try (BufferedReader br = Files.newBufferedReader(pathRaw, StandardCharsets.UTF_8)){
            String line;

            while ((line = br.readLine()) != null) {
                stringMap = jsonb.fromJson(line, HashMap.class);
            }
        }

        //read json line by line into Melon
        try (BufferedReader br = Files.newBufferedReader(pathRaw, StandardCharsets.UTF_8)){
            String line;
            while ((line = br.readLine()) != null) {
                Melon melon = jsonb.fromJson(line, Melon.class);
            }
        }

        //write object into JSON file
        Path pathToJSON = Paths.get("melons_output.json");
        jsonb.toJson(melonMap, Files.newBufferedWriter(pathToJSON, StandardCharsets.UTF_8, StandardOpenOption.CREATE,
                StandardOpenOption.WRITE));

        //==========================================Jackson=============================================
        ObjectMapper mapper = new ObjectMapper();

        //read melons_array as Array
        Melon[] melonsArrayOM = mapper.readValue(Files.newBufferedReader(pathArray, StandardCharsets.UTF_8), Melon[].class);

        //read melons_array as list
        List<Melon> melonListOM = mapper.readValue(Files.newBufferedReader(pathArray, StandardCharsets.UTF_8), ArrayList.class);

        //read melons_map as map
        Map<String, Melon> melonMapOM = mapper.readValue(Files.newBufferedReader(pathMap, StandardCharsets.UTF_8), HashMap.class);

        //read json line by line into map
        Map<String, Melon> stringMapOM = new HashMap<>();
        try (BufferedReader br = Files.newBufferedReader(pathRaw, StandardCharsets.UTF_8)){
            String line;

            while ((line = br.readLine()) != null) {
                stringMap = mapper.readValue(line, HashMap.class);
            }
        }

        //read json line by line into Melon
        try (BufferedReader br = Files.newBufferedReader(pathRaw, StandardCharsets.UTF_8)){
            String line;
            while ((line = br.readLine()) != null) {
                Melon melon = mapper.readValue(line, Melon.class);
            }
        }

        //write object into JSON file
        Path pathToJSONOM = Paths.get("melons_outputOM.json");
        mapper.writeValue(Files.newBufferedWriter(pathToJSONOM, StandardCharsets.UTF_8, StandardOpenOption.CREATE,
                StandardOpenOption.WRITE), melonMap);
    }

    //read csv files
    public static List<List<String>> readCSVAsObject(Path path, Charset charset, String delimiter) throws IOException {
        List<List<String>> content = new ArrayList<>();

        try (BufferedReader br = Files.newBufferedReader(path, charset)) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(Pattern.quote(delimiter));
                content.add(Arrays.asList(values));
            }
        }
        return content;
    }

    public static List<Melon> readAsMelon(Path path, Charset charset, String delimiter) throws IOException {
        List<Melon> content = new ArrayList<>() ;
        try (BufferedReader br = Files.newBufferedReader (path, charset)) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(Pattern.quote(delimiter));
                content.add(new Melon(values[0], Integer.parseInt(values[1])));
            }
        }
        return content;
    }



}
