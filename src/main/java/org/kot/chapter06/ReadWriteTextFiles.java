package org.kot.chapter06;

import java.io.*;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

public class ReadWriteTextFiles {
    public static void main(String[] args) throws IOException {
        Path polishFile = Paths.get("Z:/PolishFile.txt");

        //read with buffer, line by line
        try (BufferedReader br = new BufferedReader(
                new FileReader(polishFile.toFile(), StandardCharsets.UTF_16))) {
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        //Files.newBufferReader()
        try (BufferedReader br = Files.newBufferedReader(polishFile, StandardCharsets.UTF_16)) {
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        //with FileInputStream
        try (BufferedReader br = new BufferedReader(new InputStreamReader(
                new FileInputStream(polishFile.toFile()), StandardCharsets.UTF_16))) {
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        //Reading files in memory, good for not big files
        List<String> lines = Files.readAllLines(polishFile, Charset.defaultCharset());
        String content = Files.readString(polishFile);

        //for big files in GB
        try (FileChannel fileChannel = (FileChannel.open(polishFile, EnumSet.of(StandardOpenOption.READ)))) {
            MappedByteBuffer mBuffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, 0, fileChannel.size());
            if (mBuffer != null) {
                String bufferContent = StandardCharsets.UTF_8.decode(mBuffer).toString();
                System.out.println(bufferContent);
                mBuffer.clear();
            }
        }

        //for extremely big files
        final int MAP_SIZE = 5242880;

        try (FileChannel fileChannel = (FileChannel.open(polishFile, EnumSet.of(StandardOpenOption.READ)))){

            int position = 0;
            long length = fileChannel.size();

            while (position < length) {
                long remaining = length - position;
                int bytesToMap = (int) Math.min(MAP_SIZE, remaining);

                MappedByteBuffer mBuffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, position, bytesToMap);
                //logic

                position += bytesToMap;
            }
        }

        //BufferedWriter
        Path textFile = Paths.get("sample.txt");
        try (BufferedWriter bWriter = Files.newBufferedWriter(
                textFile, StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.WRITE)){
            bWriter.write("Hey, I'm pretty and I know it!");
            bWriter.newLine();
            bWriter.write("I love songs");
        }

        //Files.write
        List<String> linesToWrite = Arrays.asList("abc", "def", "ghi");
        Path textFile2 = Paths.get("sample2.txt");
        Files.write(textFile, linesToWrite, StandardCharsets.UTF_8,
                StandardOpenOption.CREATE, StandardOpenOption.WRITE);

        //FileWrite with huge files
        Path textFile3 = Paths.get("sample3.txt");
        CharBuffer cb = CharBuffer.wrap("Lorem ipsum dolor sit amet, ...");
        try (FileChannel fileChannel = (FileChannel) Files.newByteChannel(
                textFile, EnumSet.of(StandardOpenOption.CREATE,
                        StandardOpenOption.READ, StandardOpenOption.WRITE))) {

            MappedByteBuffer mBuffer = fileChannel.map(FileChannel.MapMode.READ_WRITE, 0, cb.length());
            if (mBuffer != null) {
                mBuffer.put(StandardCharsets.UTF_8.encode(cb));
            }
        }
    }
}
