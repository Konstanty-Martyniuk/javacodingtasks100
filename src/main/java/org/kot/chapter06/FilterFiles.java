package org.kot.chapter06;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;

import static java.nio.file.LinkOption.NOFOLLOW_LINKS;

public class FilterFiles {
    public static void main(String[] args) throws IOException {
        //filer files one lvl depth
        Path path = Paths.get("D:/learning/books/spring");

        //filter with glob
        try (DirectoryStream<Path> ds = Files.newDirectoryStream(path, "*.{png, jpg, web}")) {
            for (var file : ds) {
                System.out.println(file.getFileName());
            }
        }

        //DirectoryStream.Filter for files
        DirectoryStream.Filter<Path> sizeFilter = new DirectoryStream.Filter<Path>() {
            @Override
            public boolean accept(Path entry) throws IOException {
                return (Files.size(path) > 1024 * 1024 * 10);
            }
        };

        DirectoryStream.Filter<Path> sizeFilterLambda = p -> (Files.size(p) > 1024 * 1024 * 10);

        try (DirectoryStream<Path> ds = Files.newDirectoryStream(path, sizeFilterLambda)) {
            for (var file : ds) {
                System.out.println(file.getFileName());
            }
        }

        //filter for directories
        DirectoryStream.Filter<Path> folderFilter = p -> (Files.isDirectory(p, NOFOLLOW_LINKS));

        //filter for files, which were modified today
        DirectoryStream.Filter<Path> modifiedTodayFilter = new DirectoryStream.Filter<Path>() {
            @Override
            public boolean accept(Path entry) throws IOException {
                FileTime lastModified = Files.readAttributes(path, BasicFileAttributes.class).lastModifiedTime();
                LocalDate lastModifiedDate = lastModified.toInstant().atOffset(ZoneOffset.UTC).toLocalDate();
                LocalDate today = Instant.now().atOffset(ZoneOffset.UTC).toLocalDate();
                return lastModifiedDate.equals(today);
            }
        };

        //filter for hidden files/directories
        DirectoryStream.Filter<Path> hiddenFilterLambda = entry -> Files.isHidden(entry);
        DirectoryStream.Filter<Path> hiddenFilterMR = Files::isHidden;

        //FilenameFilter, for example *.pdf
        String[] files = path.toFile().list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String fileName) {
                return fileName.endsWith(".pdf");
            }
        });

        //with lambda
        FilenameFilter filterPDF = (f, n) -> n.endsWith(".pdf");
        String[] filesLambdaPDF = path.toFile().list(filterPDF);


        //FileFilter, here filtering only folders
        File[] foldersLambda = path.toFile (). listFiles (f -> f.isDirectory());
        File[] folders = path.toFile().listFiles(File::isDirectory);


    }
}
