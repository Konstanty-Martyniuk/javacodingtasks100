package org.kot.chapter06;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.List;
import java.util.Scanner;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class UseScanner {
    public static void getDoubleFromFileWithOnlyDoubles() throws IOException {
        try (Scanner scanner = new Scanner(Path.of("doubles.txt"), StandardCharsets.UTF_8)) {
            while (scanner.hasNextDouble()) {
                double number = scanner.nextDouble();
                System.out.println(number);
            }
        }
    }

    public static void getDataFromFile() throws IOException {
        try (Scanner scanner = new Scanner(Path.of("people.txt"), StandardCharsets.UTF_8).useDelimiter("[;,]")) {
            while (scanner.hasNextLine()) {
                System.out.println("Name: " + scanner.next().trim());
                System.out.println ("Surname: " + scanner.next());
                System.out.println("Age: " + scanner.nextInt());
                System.out.println("City: " + scanner.next());
            }
        }
    }

    public static void betterGetDataFromFile() throws IOException {
        try (Scanner scanner = new Scanner(Path.of("people.txt"), StandardCharsets.UTF_8).useDelimiter("[;,]")) {
            scanner.tokens().forEach(t -> System.out.println(t.trim()));
        }
    }

    public static void getPeople40() throws IOException {
        try (Scanner scanner = new Scanner(Path.of("people.txt"), StandardCharsets.UTF_8)){
            Pattern pattern = Pattern.compile("4[0-9]");

            List<String> ages = scanner.findAll(pattern)
                    .map(MatchResult::group)
                    .toList();

            System.out.println("Ages: " + ages);
        }
    }

    public static void main(String[] args) throws IOException {
        getPeople40();
    }
}
