package org.kot.chapter06;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PathToFile {
    //file location С:\learning\packt\JavaModernChallenge.pdf

    // Relative path
    Path path = Paths.get("learning/packt/JavaModernChallenge.pdf");
    Path pathConcat = Paths.get("learning", "packt/JavaModernChallenge.pdf");

    Path pathOf = Path.of("learning/packt/JavaModernChallenge.pdf:");
    Path PathOfConcat = Path.of("learning", "packt/JavaModernChallenge.pdf");

    Path pathViaFileSystemsConcat = FileSystems.getDefault().getPath("learning/packt/", "JavaModernChallenge.pdf");
    Path pathViaFileSystems = FileSystems.getDefault().getPath("learning/packt/JavaModernChallenge.pdf");

    Path pathViaUriGet = Paths.get(URI.create("file:///learning/packt/JavaModernChallenge.pdf"));
    Path pathViaUriOf = Path.of(URI.create("file:///learning/packt/JavaModernChallenge.pdf"));

    // Absolute path
    Path pathAbsolute = Paths.get("C:/learning/packt/JavaModernChallenge.pdf");

    // Normalized path
    Path pathNormalized = Paths.get("C:/learning/./packt/chapters/../JavaModernChallenge.pdf").normalize();

    // How to get separator
    private static final String FILE_SEPARATOR = File.separator;
    private static final String FILE_SEPARATOR2 = FileSystems.getDefault().getSeparator();

    // for local path
    Path pathWithSeparator = Paths.get(String.join(FILE_SEPARATOR, "learning",
            "packt", "JavaModernChallenge.pdf"));

    // for root path
    Path pathRootWithSeparatot = Paths.get(FILE_SEPARATOR + "learning",
            "packt", "JavaModernChallenge.pdf");

    // for absolute path
    Path pathAbsoluteSeparator = Paths.get(File.listRoots()[0] + "learning",
            "packt", "JavaModernChallenge.pdf");

    //getRootDirectories FileSystems.getDefault().getRootDirectories();


    //get file name from uri or url
    // JavaModernChallenge.pdf
    URI uri = URI.create("https://www.learning.com/packt/JavaModernChallenge.pdf");
    Path URIToPath = Paths.get(uri.getPath()).getFileName();
    // JavaModernChallenge.pdf
    URL url = new URL("https://www.learning.com/packt/JavaModernChallenge.pdf");
    Path URLToPath = Paths.get(uri.getPath()).getFileName();

    public PathToFile() throws IOException {
    }

    //convert path to absolute
    Path pathToAbsolute = path.toAbsolutePath();

    //convert to real path

    Path pathToRealPath = path.toRealPath();
}
