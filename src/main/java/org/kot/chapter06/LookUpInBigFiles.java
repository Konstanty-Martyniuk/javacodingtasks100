package org.kot.chapter06;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Scanner;
import java.util.regex.Pattern;

public class LookUpInBigFiles {

    private static final int MAP_SIZE = 5242880;

    private static int countStringInString(String text, String toFind) {
        return text.split(Pattern.quote(toFind), -1).length - 1;
    }

    //BufferReader
    public static int countOccurrences(Path path, String toFind, Charset charset) throws IOException {
        int count = 0;
        try (BufferedReader br = Files.newBufferedReader(path, charset)) {
            String line;
            while ((line = br.readLine()) != null) {
                count+=countStringInString(line, toFind);
            }
        }
        return count;
    }

    //Files.ReadAllLines() if RAM is not an issue
    public static int countOccurrencesRAL(Path path, String toFind, Charset charset) throws IOException {
        return Files.readAllLines(path, charset).parallelStream()
                .mapToInt((p) -> countStringInString(p, toFind))
                .sum();
    }

    //Files.lines()
    public static int countOccurrencesFl(Path path, String toFind, Charset charset) throws IOException {
        return Files.lines(path, charset).parallel()
                .mapToInt((p) -> countStringInString(p, toFind))
                .sum();
    }

    //Scanner
    public static long countOccurrencesS(Path path, String toFind, Charset charset) throws IOException {
        long count;

        try (Scanner scanner = new Scanner(path, charset)
                .useDelimiter(Pattern.quote(toFind))) {
            count = scanner.tokens().count() - 1;
        }
        return count;
    }

    //MappedByteBuffer for small files load whole file, for bigger use 5mb chunks
    public static int countOccurrencesMBB(Path path, String toFind, Charset charset) throws IOException {
        final byte[] textToFind = toFind.getBytes(StandardCharsets.UTF_8);
        int count = 0;

        try (FileChannel fileChannel = FileChannel.open(path, StandardOpenOption.READ)){
            int position = 0;
            long length = fileChannel.size();

            while (position < length) {

                long remaining = length - position;
                long bytestomap = (long) Math.min(MAP_SIZE, remaining);
                MappedByteBuffer mbBuffer = fileChannel.map(FileChannel.MapMode.READ_ONLY, position, bytestomap);

                long limit = mbBuffer.limit();
                long lastSpace = -1;
                long firstChar = -1;
                while (mbBuffer.hasRemaining()) {

                    boolean isFirstChar = false;
                    while (firstChar != 0 && mbBuffer.hasRemaining()) {

                        byte currentByte = mbBuffer.get();

                        if (Character.isWhitespace((char) currentByte)) {
                            lastSpace = mbBuffer.position();
                        }

                        if (textToFind[0] == currentByte) {
                            isFirstChar = true;
                            break;
                        }
                    }

                    if (isFirstChar) {

                        boolean isRestOfChars = true;

                        int j;
                        for (j = 1; j < textToFind.length; j++) {
                            if (!mbBuffer.hasRemaining() || textToFind[j] != mbBuffer.get()) {
                                isRestOfChars = false;
                                break;
                            }
                        }

                        if (isRestOfChars) {
                            count++;
                            lastSpace = -1;
                        }

                        firstChar = -1;
                    }
                }

                if (lastSpace > -1) {
                    position = (int) (position - (limit - lastSpace));
                }

                position += bytestomap;
            }
        }
        return count;
    }


    public static void main(String[] args) {
        String text = "hello world hello hello";
        String pattern = "hello";
        System.out.println(countStringInString(text, pattern));
    }
}
