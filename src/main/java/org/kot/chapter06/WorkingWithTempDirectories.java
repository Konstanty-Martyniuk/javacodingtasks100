package org.kot.chapter06;

import java.io.IOException;
import java.nio.file.*;

public class WorkingWithTempDirectories {

    public static void main(String[] args) throws IOException {
        //how ot find temp folder
        String defaultBaseDir = System.getProperty("java.io.tmpdir");
        //***********************CREATION************************************
        //=======================DIRECTORIES==================================

        //create temp directory in default OS location
        // C:\Users\Anghel\AppData\Local\Temp\8083202661590940905
        Path tmpNoPrefix = Files.createTempDirectory(null);

        //create temp directory in default OS location with prefix
        // C:\Users\Anghel\AppData\Local\Temp\logs_8083202661590940905
        String customDirPrefix = "logs_";
        Path temPrefixDir = Files.createTempDirectory(customDirPrefix);

        //create temp directory in desired location with prefix
        // D:\temp\logs_8083202661590940905
        Path customDirLocation = FileSystems.getDefault().getPath("D:/tmp");
        Path tempCustomLocationCustomPrefix = Files.createTempDirectory(customDirLocation, customDirPrefix);

        //=======================FILES==================================

        //create temp file in default OS location
        // C:\Users\Anghel\AppData\Local\Temp\8083202661590940905
        Path tmpFileNoPrefix = Files.createTempFile(null, null);

        //create temp directory in default OS location with prefix and postfix
        // C:\Users\Anghel\AppData\Local\Temp\logs_8083202661590940905
        String customFilePrefix = "log_";
        String customFilePostfix = ".txt";
        Path tempPrefixAndPostfixFile = Files.createTempFile(customFilePrefix, customFilePostfix);

        //create temp file in desired location with prefix and postfix
        // D:\temp\logs_8083202661590940905
        Path customFileLocation = FileSystems.getDefault().getPath("D:/tmp");
        Path tmpCustomFileLocationCustomPrefix = Files.createTempFile(customDirLocation, customFilePrefix, customFilePostfix);

        //***********************DELETION************************************
        try {
            Path tempDir = Files.createTempDirectory(customDirLocation, customDirPrefix);
            Path tempFile1 = Files.createTempFile(tempDir, customFilePrefix, customFilePostfix);
            Path tempFile2 = Files.createTempFile(tempDir, customFilePrefix, customFilePostfix);

            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    try (DirectoryStream<Path> ds = Files.newDirectoryStream(tempDir)) {
                        for (var file : ds) {
                            Files.delete(file);
                        }
                        Files.delete(tempDir);
                    } catch (IOException e) {
                        throw new RuntimeException(e);
                    }
                }
            });
        } finally {
            System.out.println("Temp folder and files were deleted");
        }
    }
}
