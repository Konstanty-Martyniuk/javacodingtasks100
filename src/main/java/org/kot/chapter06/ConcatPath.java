package org.kot.chapter06;

import java.nio.file.Path;
import java.nio.file.Paths;

public class ConcatPath {

    public static void main(String[] args) {
        Path base = Paths.get("D:/learning/packt");

        //adding file name
        Path pathToBook = base.resolve("JBossTools.pdf");
        //another file in the same location
        Path pathTo2ndBook = base.resolve("OneMoreNiceBook.pdf");

        String[] books = {"Book1.pdf", "Book2.pdf", "Book3.pdf"};

        for (var book : books) {
            System.out.println(base.resolve(book));
        }


        //replace file with another at a path
        Path pathWithFile = Paths.get("D:/learning/packt/oldbook.pdf");

        Path pathWithNewBook = pathWithFile.resolveSibling("NewBook.pdf");

        //get "base" from filepath
        Path parentPath = pathWithFile.getParent();


        //relative path
        Path path1 = Paths.get("/home/user/Documents/file.txt");
        Path path2 = Paths.get("/home/user/Desktop");

        Path relativePath = path2.relativize(path1);  //../Documents/file.txt
        System.out.println(relativePath);

        Path path3 = Paths.get("/learning/packt/2003/JBossTools3.pdf");
        Path path4 = Paths.get("/learning/packt/2019");

        Path path3ToPath4 = path3.relativize(path4);
        Path path4ToPath3 = path4.relativize(path3);
        System.out.println("path3ToPath4 - " + path3ToPath4);
        System.out.println("path4ToPath3 - " + path4ToPath3);
    }
}
