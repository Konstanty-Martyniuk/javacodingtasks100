package org.kot.chapter06;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class FileTokenizer {
    public static void main(String[] args) {
        Path path = Paths.get("clothes.txt");
        //clothes.txt contains structure: name of product | colour \ available amount / size
        //dividers: &атр;, |, \, /

    }
    //String.split()
    public static List<String> get(Path path, Charset charset, String delimiter) throws IOException {
        String delimiterStr = Pattern.quote(delimiter);
        List<String> content = new ArrayList<>();

        try (BufferedReader br = Files.newBufferedReader(path, charset)) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(delimiterStr);
                content.addAll(Arrays.asList(values));
            }
        }
        return content;
    }
    //Stream + toList()
    public static List<String> getWithStream(Path path, Charset charset, String delimiter) throws IOException {
        String delimiterStr = Pattern.quote(delimiter);
        List<String> content = new ArrayList<>();

        try (BufferedReader br = Files.newBufferedReader(path, charset)) {
            String line;
            while ((line = br.readLine()) != null) {
                content.addAll(Stream.of(line.split(delimiterStr))
                        .toList());
            }
        }
        return content;
    }

    //Files.lines()
    public static List<String> lazyGetWithStream(Path path, Charset charset, String delimiter) throws IOException {
        try (Stream<String> lines = Files.lines(path, charset)){
            return lines
                    .map(l -> l.split(Pattern.quote(delimiter)))
                    .flatMap(Arrays::stream)
                    .toList();
        }
    }

    //small files solution
    public static List<String> getForSmallFiles(Path path, Charset charset, String delimiter) throws IOException {
        return Files.readAllLines(path, charset).stream()
                .map(l -> l.split(Pattern.quote(delimiter)))
                .flatMap(Arrays::stream)
                .toList();
    }

    public static List<String> getWithMultipleDelimiters(Path path, Charset charset, String...delimiters) throws IOException {
        String[] escapeDelimiters = new String[delimiters.length];
        Arrays.setAll(escapeDelimiters, t -> Pattern.quote(delimiters[t]));
        String delimiterSet = String.join("|", escapeDelimiters);

        List<String> content = new ArrayList<>();

        try (BufferedReader br = Files.newBufferedReader(path, charset)) {
            String line;

            while ((line = br.readLine()) != null) {
                String[] values = line.split(delimiterSet);
                content.addAll(Arrays.asList(values));
            }
        }
        return content;
    }

}
