package org.kot.chapter06;

import java.io.IOException;
import java.nio.file.*;

public class PathVisitor extends SimpleFileVisitor<Path> {
    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException ioException) throws IOException {
        if (ioException != null) throw ioException;
        System.out.println("Visited directory: " + dir);

        return FileVisitResult.CONTINUE;
    }

    public static void main(String[] args) throws IOException {
        Path path = Paths.get("C:/projects");
        PathVisitor visitor =  new PathVisitor();
        Files.walkFileTree(path, visitor);
    }

}
