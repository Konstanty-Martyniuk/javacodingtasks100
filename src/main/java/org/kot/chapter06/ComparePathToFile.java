package org.kot.chapter06;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ComparePathToFile {
    public static void main(String[] args) throws IOException {
        Path path1 = Paths.get("/learning/packt/JavaModernChallenge.pdf");
        Path path2 = Paths.get("/LEARNING/PACKT/JavaModernChallenge.pdf");
        Path path3 = Paths.get("D:/learning/packt/JavaModernChallenge.pdf");

        //Path.equals
        boolean path1EqualsPath2 = path1.equals(path2); //true
        boolean path2EqualsPath3 = path2.equals(path3); //false
        System.out.println(path1EqualsPath2);
        System.out.println(path2EqualsPath3);

        //Files.isSameFile
        boolean path1IsSameFilePath2 = Files.isSameFile(path1, path2);  //true
        boolean path1IsSameFilePath3 = Files.isSameFile(path1, path3); //true
        boolean path2IsSameFilePath3 = Files.isSameFile(path2, path3); //true

        //Lexicographic comparing Path.compareTo() is good for sorting. 0 means equal, < 0 smaller, > 0 bigger than arg
        int path1CompareToPath2 = path1.compareTo(path2);
        int path2CompareToPath3 = path2.compareTo(path3);

        System.out.println("path1CompareToPath2: " + path1CompareToPath2); // 0
        System.out.println("path2CompareToPath3: " + path2CompareToPath3); // 24
        //in logic use  if (path2CompareToPath3 > 0| == 0 | < 0)

        //partial comparing Path.startsWith() Path.endsWith()
        boolean sw = path1.startsWith("/learning/packt"); // true
        boolean ew = path1.endsWith("JavaModernChallenge.pdf"); // true
    }
}
