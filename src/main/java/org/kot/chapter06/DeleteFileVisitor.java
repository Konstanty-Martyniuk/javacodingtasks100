package org.kot.chapter06;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.EnumSet;

public class DeleteFileVisitor implements FileVisitor {

    public static boolean delete(Path file) throws IOException {
        return Files.deleteIfExists(file);
    }

    @Override
    public FileVisitResult preVisitDirectory(Object dir, BasicFileAttributes attrs) throws IOException {
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult visitFile(Object file, BasicFileAttributes attrs) throws IOException {
        return null;
    }

    @Override
    public FileVisitResult visitFileFailed(Object file, IOException exc) throws IOException {
        return FileVisitResult.CONTINUE;
    }

    @Override
    public FileVisitResult postVisitDirectory(Object dir, IOException exc) throws IOException {
        delete((Path) dir);
        return FileVisitResult.CONTINUE;
    }

    public static void main(String[] args) throws IOException {
        Path directory = Paths.get("Z:/fakeDir");
        DeleteFileVisitor deleteFileVisitor = new DeleteFileVisitor();
        var opts = EnumSet.of(FileVisitOption.FOLLOW_LINKS);
        Files.walkFileTree(directory, opts, Integer.MAX_VALUE, deleteFileVisitor);
    }
}
