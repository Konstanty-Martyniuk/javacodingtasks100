package org.kot.chapter06;

import java.io.IOException;
import java.nio.file.*;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.stream.Stream;

public class FindFilesDirs {
    private static final Path startPath = Paths.get("D:/learning");

    public static void main(String[] args) throws IOException {
        //find files with .properties
        Stream<Path> resultAsStream = Files.find(
                startPath,
                Integer.MAX_VALUE,
                (path, attr) -> path.toString().endsWith(".properties"),
                FileVisitOption.FOLLOW_LINKS);

        //find files starts with application
        Stream<Path> findingsAsStream = Files.find(
                startPath,
                Integer.MAX_VALUE,
                (path, attr) -> attr.isRegularFile() && path.getFileName().toString().startsWith("application")
        );

        //find all dir created after 19/03/2019
        Stream<Path> resultDirsAsStream = Files.find(
                startPath,
                Integer.MAX_VALUE,
                (path, attr) -> attr.isDirectory() && attr.creationTime().toInstant()
                        .isAfter(LocalDate.of(2019, 3, 16).atStartOfDay()
                                .toInstant(ZoneOffset.UTC))
        );
    }

    //PathMatcher
    public static Stream<Path> fetchFilesMatching(Path root, String syntaxPattern) throws IOException {
        final PathMatcher matcher = root.getFileSystem().getPathMatcher(syntaxPattern);

        return Files.find(
                root,
                Integer.MAX_VALUE,
                (path, attr) -> matcher.matches(path) && !attr.isDirectory());
    }
}
