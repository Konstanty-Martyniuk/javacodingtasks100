package org.kot.chapter06;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class ReadFileFromStream {
    private static final String FILE_PATH = "D:/learning/packt/resources.txt";

    public static void main(String[] args) {
        try (Stream<String> fileStream = Files.lines(Paths.get(FILE_PATH), StandardCharsets.UTF_8)){
            fileStream.forEach(System.out::println);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        try (BufferedReader br = Files.newBufferedReader(Paths.get(FILE_PATH), StandardCharsets.UTF_8)) {
            br.lines().forEach(System.out::println);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
