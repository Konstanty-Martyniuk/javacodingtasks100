package org.kot.chapter06;

import lombok.Data;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.EnumSet;

@Data
public class SearchFileVisitor implements FileVisitor {
    private final Path fileNameToSearch;
    private boolean fileFound;

    public SearchFileVisitor(Path fileNameToSearch) {
        this.fileNameToSearch = fileNameToSearch;
    }

    private boolean search(Path file) throws IOException {
        Path fileName = file.getFileName();
        if (fileNameToSearch.equals(fileName)) {
            System.out.println("File was found: " + fileNameToSearch + " in " + file.toRealPath());
            return true;
        }
        return false;
    }


    @Override
    public FileVisitResult visitFile(Object file, BasicFileAttributes attrs) throws IOException {
        fileFound = search((Path) file);

        if (!fileFound) {
            return FileVisitResult.CONTINUE;
        } else {
            return FileVisitResult.TERMINATE;
        }
    }

    @Override
    public FileVisitResult visitFileFailed(Object file, IOException exc) throws IOException {
        return FileVisitResult.CONTINUE;
    }

    //Use preVisitDirectory or postVisitDirectory to search folders
    @Override
    public FileVisitResult preVisitDirectory(Object dir, BasicFileAttributes attrs) throws IOException {
        return null;
    }

    @Override
    public FileVisitResult postVisitDirectory(Object dir, IOException exc) throws IOException {
        return null;
    }

    public static void main(String[] args) throws IOException {
        Path searchFile = Paths.get("angular.pdf");
        SearchFileVisitor searchFileVisitor = new SearchFileVisitor(searchFile);

        var opts = EnumSet.of(FileVisitOption.FOLLOW_LINKS);
        Iterable<Path> roots = FileSystems.getDefault().getRootDirectories();

        for (var root : roots) {
            if (!searchFileVisitor.isFileFound()) {
                Files.walkFileTree(root, opts, Integer.MAX_VALUE, searchFileVisitor);
            }
        }
    }
}
