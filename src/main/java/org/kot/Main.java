package org.kot;

import com.rits.cloning.Cloner;
import org.apache.commons.lang3.SerializationUtils;
import org.kot.NumbersCharsLines.FirstNonRepeatedCharacter;
import org.kot.NumbersCharsLines.ReversCharsAndWords;
import org.kot.SwitchTasks.ImmutablePoint;
import org.kot.SwitchTasks.PointDeepCloning;
import org.kot.SwitchTasks.Radius;
import org.kot.SwitchTasks.User;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.kot.NumbersCharsLines.CountsAndContains.betterCountOccurrencesOfStringInStringWithPattern;
import static org.kot.NumbersCharsLines.CountsAndContains.countOccurrencesOfStringInString;
import static org.kot.NumbersCharsLines.FindMaxOccurrence.findMaxOccurrence;
import static org.kot.NumbersCharsLines.FindMaxOccurrence.findMaxOccurrenceWithASCIICodes;

public class Main {
    public static void main(String[] args) {
        FirstNonRepeatedCharacter methods = new FirstNonRepeatedCharacter();
        System.out.println(methods.findFirstNonRepeatedCharacter("aaaKreatu"));
        ReversCharsAndWords reversCharsAndWords = new ReversCharsAndWords();
        System.out.println(reversCharsAndWords.reverseCharsAndWordsWithStream("I like to drink coffee in the morning."));
        System.out.println(findMaxOccurrenceWithASCIICodes("Who the hell created this task?"));
        System.out.println(findMaxOccurrence("Who the hell created this task?"));
        System.out.println(countOccurrencesOfStringInString("111", "11"));
        System.out.println(betterCountOccurrencesOfStringInStringWithPattern("111", "11"));
        Radius radius = new Radius();
        radius.setStart(0);
        radius.setEnd(10);

        ImmutablePoint point = new ImmutablePoint(1.0, 2.0, radius);
        System.out.println(point);
        point.getRadius().setStart(5);
        System.out.println(point);

        User user = User.getBuilder("kot", "123").build();
        User user1 = User.getBuilder("kot", "123")
                .email("dssd@gmail.com")
                .firstName("John").build();

        PointDeepCloning pointDeepCloning = new PointDeepCloning(1.0, 2.0, radius);
        Cloner cloner = new Cloner();
        PointDeepCloning clonedPoint = cloner.deepClone(pointDeepCloning);
        PointDeepCloning clonedPoint1 = SerializationUtils.clone(pointDeepCloning);
    }

    public static ArrayList<Integer> convertNamesToLengths(List<String> names) {
        return names.stream()
                .map(String::length)
                .collect(Collectors.toCollection(ArrayList::new));
    }
}
