package org.kot.DateAndTime;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class DateMain {
    public static void main(String[] args) {
        //dd-MM-yyyy
        LocalDate localDate = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        String stringLD = formatter.format(localDate);

        //HH:mm:ss
        LocalTime localTime = LocalTime.now();
        DateTimeFormatter formatterTime = DateTimeFormatter.ofPattern("HH:mm:ss");
        String stringLT = formatterTime.format(localTime);

        //short version
        String localTimeShort = LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss"));

        //dd-MM-yyyy HH:mm:ss
        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter formatterDateTime = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        String stringLDT = formatterDateTime.format(localDateTime);
    }
}
