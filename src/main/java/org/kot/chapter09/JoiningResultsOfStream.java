package org.kot.chapter09;

import org.kot.chapter05.MutableMelon;

import java.util.List;
import java.util.stream.Collectors;

public class JoiningResultsOfStream {
    public static void main(String[] args) {
        List<MutableMelon> melons = List.of(new MutableMelon("Gac", 2000),
                new MutableMelon("Hemi", 1600),
                new MutableMelon("Cantaloupe", 3000),
                new MutableMelon("Gac", 2200),
                new MutableMelon("Horned", 1500),
                new MutableMelon("Apollo", 2000),
                new MutableMelon("Horned", 1700));

        //concat names

        String melonNames = melons.stream()
                .map(MutableMelon::getType)
                .distinct()
                .sorted()
                .collect(Collectors.joining(", ", "Available melons: ", ". Thx for visiting!"));
        //Available melons: Apollo, Cantaloupe, Gac, Hemi, Horned. Thx for visiting!

        System.out.println(melonNames);
    }
}
