package org.kot.chapter09.FindInStream;

import java.util.Arrays;
import java.util.List;

public class AnyNoneAllMatch {
    public static void main(String[] args) {
        List<String> melons = Arrays.asList(
                "Gac", "Cantaloupe", "Hemi", "Gac", "Gac",
                "Hemi", "Cantaloupe", "Horned", "Hemi", "Hemi");

        boolean isAnyGac = melons.stream()
                .anyMatch(m -> m.equalsIgnoreCase("gac"));

        boolean isNoneGac = melons.stream()
                .noneMatch(m -> m.equalsIgnoreCase("gac"));

        boolean areAllGac = melons.stream()
                .allMatch(m -> m.equals("Gac"));
    }
}
