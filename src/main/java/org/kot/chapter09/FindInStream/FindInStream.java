package org.kot.chapter09.FindInStream;

import org.kot.chapter05.MutableMelon;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class FindInStream {
    public static void main(String[] args) {
        List<String> melons = Arrays.asList(
                "Gac", "Cantaloupe", "Hemi", "Gac", "Gac",
                "Hemi", "Cantaloupe", "Horned", "Hemi", "Hemi");

        //findAny()
        Optional<String> anyMelon = melons.stream()
                .findAny();

        if (!anyMelon.isEmpty()) {
            System.out.println("Any melon: " + anyMelon.get());
        } else {
            System.out.println("No melons :(");
        }

        String anyApollo = melons.stream()
                .filter(m -> m.equalsIgnoreCase("apollo"))
                .findAny()
                .orElse("no apollo found");


        //findFirst
        Optional<String> firstMelon = melons.stream().findFirst();
    }
}
