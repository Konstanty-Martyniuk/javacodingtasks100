package org.kot.chapter09.FindInStream;

import org.kot.chapter05.MutableMelon;

import java.util.*;
import java.util.stream.Collectors;
import java.util.Map.Entry;
import java.util.stream.Stream;

public class CollectResults {
    public static void main(String[] args) {
        List<MutableMelon> melons = List.of(new MutableMelon("Gac", 2000),
                new MutableMelon("Hemi", 1600),
                new MutableMelon("Gac", 3000),
                new MutableMelon("Apollo", 2000),
                new MutableMelon("Horned", 1700));

        /*
        LIST
         */
        //java 16+ immutable
        List<Integer> resultToList = melons.stream()
                .map(MutableMelon::getWeight)
                .filter(m -> m >= 1000)
                .toList();

        //mutable
        List<Integer> resultToList2 = melons.stream()
                .map(MutableMelon::getWeight)
                .filter(m -> m >= 1000)
                .collect(Collectors.toCollection(ArrayList::new));

        /*
        SET
         */
        //just set
        Set<Integer> resultToSet = melons.stream ()
                .map(MutableMelon::getWeight)
                .filter(x -> x >= 1000)
                .collect(Collectors.toSet());

        //exact type of set - HashSet
        Set<Integer> resultToSet2 = melons.stream()
                .map(MutableMelon::getWeight)
                .filter(x -> x >= 1000)
                .collect(Collectors.toCollection(HashSet::new));

        //filter and collect uniq and sort asc in TreeSet
        Set<Integer> resultToTreeSet = melons.stream()
                .map(MutableMelon::getWeight)
                .filter(x -> x >= 1000)
                .collect(Collectors.toCollection(HashSet::new));

        /*
        MAP
         */
        //filter and put in map
        Map<String, Integer> resultToMap = melons.stream()
                .distinct()
                .collect(Collectors.toMap(MutableMelon::getType, MutableMelon::getWeight));

        //filter and put in map with pseudoUniq key
        Map<Integer, Integer> resultToMapIntInt = melons.stream()
                .distinct()
                .map(x -> Map.entry(
                        new Random().nextInt(Integer.MAX_VALUE), x.getWeight()))
                .collect(Collectors.toMap(Entry::getKey, Entry::getValue));

        //filter and put in map, fixed java.lang.IllegalStateException
        Map<String, Integer> resultToMapFixedException = melons.stream()
                .distinct()
                .collect(Collectors.toMap(
                        MutableMelon::getType, MutableMelon::getWeight, (oldValue, newValue) -> oldValue));

        //sort by weight and put in map, fixed java.lang.IllegalStateException
        Map<String, Integer> resultToMapSorted = melons.stream()
                .sorted(Comparator.comparingInt(MutableMelon::getWeight))
                .collect(Collectors.toMap(MutableMelon::getType, MutableMelon::getWeight,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));

        //Collect word frequency using a collector toMap
        String str = "Lorem Ipsum is simply Ipsum Lorem not simply Ipsum";
        Map<String, Integer> mapOfWords = Stream.of(str)
                .map(w -> w.split("\\s+"))
                .flatMap(Arrays::stream)
                .collect(Collectors.toMap(String::toLowerCase, w -> 1, Integer::sum));
    }
}
