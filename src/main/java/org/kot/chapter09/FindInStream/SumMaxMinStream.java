package org.kot.chapter09.FindInStream;

import org.kot.chapter05.MutableMelon;

import java.util.List;

public class SumMaxMinStream {
    public static void main(String[] args) {
        List<MutableMelon> melons = List.of(new MutableMelon("Gac", 2000),
                new MutableMelon("Hemi", 1600),
                new MutableMelon("Gac", 3000),
                new MutableMelon("Apollo", 2000),
                new MutableMelon("Horned", 1700));

        int total = melons.stream()
                .mapToInt(MutableMelon::getWeight)
                .sum();

        int max = melons.stream()
                .mapToInt(MutableMelon::getWeight)
                .max()
                .orElse(-1);

        int min = melons.stream()
                .mapToInt(MutableMelon::getWeight)
                .min()
                .orElse(-1);


        /*
        REDUCE
         */

        int totalReduce = melons.stream()
                .map(MutableMelon::getWeight)
                .reduce(0, Integer::sum);

        int maxReduce = melons.stream()
                .map(MutableMelon::getWeight)
                .reduce(Integer::max)
                .orElse(-1);
    }
}
