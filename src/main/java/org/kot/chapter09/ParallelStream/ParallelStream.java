package org.kot.chapter09.ParallelStream;

import java.util.*;
import java.util.concurrent.ForkJoinPool;
import java.util.stream.DoubleStream;

public class ParallelStream {
    public static void main(String[] args) {
        Random random = new Random();
        List<Double> numbers = new ArrayList<>();

        for (int i = 0; i < 1_000_000; i++) {
            numbers.add(random.nextDouble());
        }

        //via stream
        var result = DoubleStream.generate(() -> random.nextDouble()).limit(1_000_000);

        //via mr
        var result2 = DoubleStream.generate(random::nextDouble).limit(1_000_000);

        //traditional non parallel solution
        var result3 = numbers.stream()
                .reduce(Double::sum)
                .orElse(-1d);

        //parallel solution
        var resultParallel = numbers.parallelStream()
                .reduce(Double::sum)
                .orElse(-1d);

        var resultParallel2 = numbers.stream()
                .parallel()
                .reduce(Double::sum)
                .orElse(-1d);

        int noOfProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + noOfProcessors);

        //set amount of available threads globally
        System.setProperty("java.util.concurrent.ForkJoinPoll.common.parallelism", "10");

        //To create a ForkJoinPool with 5 threads
        ForkJoinPool customPool =  new ForkJoinPool(5);

        var resultParallel3 = customPool.submit(
                () -> numbers.parallelStream()
                        .reduce((a,b) -> a + b)
                        .orElse(-1d)
        );

        /*
        SPLITERATOR
         */
        List<Integer> intNumbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        Spliterator<Integer> s1 = intNumbers.spliterator();
        Spliterator<Integer> sFromStream = intNumbers.stream().spliterator();
        s1.tryAdvance(integer -> System.out.println("1st element in s1: " + integer));
        System.out.println("Estimate size of list is: " + s1.estimateSize());
        Spliterator<Integer> s2 = s1.trySplit();
        System.out.println("Estimate size of s1: " + s1.estimateSize());
        System.out.println("Estimate size of s2: " + s1.estimateSize());

        //print remaining elements:
        s1.forEachRemaining(System.out::println);
        s2.forEachRemaining(System.out::println);

        if (s1.hasCharacteristics(Spliterator.ORDERED)) {
            System.out.println("ORDERED");
        }
        if (s1.hasCharacteristics(Spliterator.SIZED)) {
            System.out.println("SIZED");
        }
    }
}
