package org.kot.chapter09.ParallelStream;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

public class NullSafeStream {
    public static void main(String[] args) {
        List<Integer> ints = Arrays.asList(5, null, 6, null, 1, 2);

    }

    public static <T> Stream<T> collectionAsStreamWithNulls(Collection<T> collection) {
        return Stream.ofNullable(collection).flatMap(Collection::stream);
    }
}
