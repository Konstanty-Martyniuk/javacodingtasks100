package org.kot.chapter09;

import org.kot.chapter05.MutableMelon;

import java.util.*;

import static java.util.stream.Collectors.*;

public class FilteringFlatMapping {
    public static void main(String[] args) {
        List<MutableMelon> melons = List.of(new MutableMelon("Gac", 2000),
                new MutableMelon("Hemi", 1600),
                new MutableMelon("Hemi", 2200),
                new MutableMelon("Hemi", 2000),
                new MutableMelon("Cantaloupe", 3000),
                new MutableMelon("Gac", 2200),
                new MutableMelon("Horned", 1500),
                new MutableMelon("Apollo", 2000),
                new MutableMelon("Horned", 1700));

        /*
        FILTERING
         */
        //filtering method
        Map<String, Set<MutableMelon>> melonsFiltering = melons.stream()
                .collect(groupingBy(MutableMelon::getType, filtering(m -> m.getWeight() > 2000, toSet())));
        System.out.println(melonsFiltering); //{Apollo=[], Gac=[MutableMelon(type=Gac, weight=2200)], Hemi=[], Horned=[], Cantaloupe=[MutableMelon(type=Cantaloupe, weight=3000)]}

        //filter method
        Map<String, Set<MutableMelon>> melonsFilter = melons.stream()
                .filter(m -> m.getWeight() > 2000)
                .collect(groupingBy(MutableMelon::getType, toSet()));
        System.out.println(melonsFilter); //{Gac=[MutableMelon(type=Gac, weight=2200)], Cantaloupe=[MutableMelon(type=Cantaloupe, weight=3000)]}

        //filtering only Hemi and put in container <= 2000 or > 2000
        Map<Boolean, Set<MutableMelon>> filteringHemi = melons.stream()
                .collect(partitioningBy(m -> m.getWeight() > 2000,
                        filtering(m -> m.getType().equalsIgnoreCase("hemi"), toSet())));
        System.out.println(filteringHemi); //{false=[MutableMelon(type=Hemi, weight=1600), MutableMelon(type=Hemi, weight=2000)], true=[MutableMelon(type=Hemi, weight=2200)]}

        //filter will do the same result
        Map<Boolean, Set<MutableMelon>> filterHemi = melons.stream()
                .filter(m -> m.getType().equalsIgnoreCase("hemi"))
                .collect(partitioningBy(m -> m.getWeight() > 2000, toSet()));
        System.out.println(filterHemi); //{false=[MutableMelon(type=Hemi, weight=1600), MutableMelon(type=Hemi, weight=2000)], true=[MutableMelon(type=Hemi, weight=2200)]}

        /*
        MAPPING
         */
        //group melons by weight
        Map<String, TreeSet<Integer>> melonsMapping = melons.stream()
                .collect(groupingBy(MutableMelon::getType, mapping(MutableMelon::getWeight, toCollection(TreeSet::new))));
        System.out.println(melonsMapping); //{Apollo=[2000], Gac=[2000, 2200], Hemi=[1600, 2000, 2200], Horned=[1500, 1700], Cantaloupe=[3000]}

        //get melons and put in container <= 2000 or > 2000
        Map<Boolean, Set<String>> melonsMappingByWeight = melons.stream()
                .collect(partitioningBy(m -> m.getWeight() > 2000, mapping(MutableMelon::getType, toSet())));
        System.out.println(melonsMappingByWeight); //{false=[Apollo, Gac, Hemi, Horned], true=[Gac, Hemi, Cantaloupe]}

        /*
        FLATMAPPING
         */
        List<MutableMelonPests> melonsGrown = Arrays.asList(
                new MutableMelonPests("Honeydew", 5600, Arrays.asList("Spider Mites", "Melon Aphids", "Squash Bugs")),
                new MutableMelonPests("Crenshaw", 2000, Arrays.asList("Pickleworms")),
                new MutableMelonPests("Crenshaw", 1000, Arrays.asList("Cucumber Beetles", "Melon Aphids")),
                new MutableMelonPests("Gac", 4000, Arrays.asList("Spider Mites", "Cucumber Beetles")),
                new MutableMelonPests("Gac", 1000, Arrays.asList("Squash Bugs", "Squash Vine Borers")));

        //group by melons and get pests
        Map<String, Set<String>> pestsFlatMapping = melonsGrown.stream()
                .collect(groupingBy(MutableMelonPests::getType, flatMapping(m -> m.getPests().stream(), toSet())));
        System.out.println(pestsFlatMapping); //{Crenshaw=[Cucumber Beetles, Pickleworms, Melon Aphids], Gac=[Cucumber Beetles, Squash Bugs, Spider Mites, Squash Vine Borers], Honeydew=[Squash Bugs, Spider Mites, Melon Aphids]}

        //get pests for melons <= 2000 and put them in set, same  for > 2000
        Map<Boolean, Set<String>> pestsFlatMappingByWeight = melonsGrown.stream()
                .collect(partitioningBy(m -> m.getWeight() > 2000, flatMapping(m -> m.getPests().stream(), toSet())));
        System.out.println(pestsFlatMappingByWeight); //{false=[Cucumber Beetles, Squash Bugs, Pickleworms, Melon Aphids, Squash Vine Borers], true=[Squash Bugs, Cucumber Beetles, Spider Mites, Melon Aphids]}

    }
}
