package org.kot.chapter09;

import java.util.Comparator;

public class MelonComparator implements Comparator<MelonMethodReference> {
    @Override
    public int compare(MelonMethodReference o1, MelonMethodReference o2) {
        return Integer.compare(o1.getWeight(), o2.getWeight());
    }
}
