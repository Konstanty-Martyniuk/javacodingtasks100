package org.kot.chapter09;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class MelonMethodReference {
    private final String type;
    private int weight;

    static int growing100g(MelonMethodReference melon) {
        melon.setWeight(melon.getWeight() + 100);
        return melon.getWeight();
    }
}
