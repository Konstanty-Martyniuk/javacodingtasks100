package org.kot.chapter09;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Teeing {
    CountSum countSum = Stream.of(2, 11, 1, 5, 7 ,8, 12)
            .collect(Collectors.teeing(
                    Collectors.counting(),
                    Collectors.summingInt(e -> e),
                    CountSum::new));
}
