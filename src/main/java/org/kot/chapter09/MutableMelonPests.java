package org.kot.chapter09;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
@Data
@AllArgsConstructor
public class MutableMelonPests {
    private final String type;
    private final int weight;
    private final List<String> pests;

    public MutableMelonPests(String type, int weight) {
        this.type = type;
        this.weight = weight;
        this.pests = new ArrayList<>();
    }
    @Override
    public String toString() {
        return type + "(" + weight + "g)" + (!pests.isEmpty() ? " " + pests : "");
    }

}
