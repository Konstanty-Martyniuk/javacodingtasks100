package org.kot.chapter09.Collector;

import org.kot.chapter05.MutableMelon;

import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.partitioningBy;

public class CustomCollector {
    public static void main(String[] args) {
        List<MutableMelon> melons = List.of(new MutableMelon("Gac", 2000),
                new MutableMelon("Hemi", 1600),
                new MutableMelon("Hemi", 2200),
                new MutableMelon("Hemi", 2000),
                new MutableMelon("Cantaloupe", 3000),
                new MutableMelon("Gac", 2200),
                new MutableMelon("Horned", 1500),
                new MutableMelon("Apollo", 2000),
                new MutableMelon("Horned", 1700));

        Map<Boolean, List<MutableMelon>> byWeight = melons.stream()
                .collect(partitioningBy(m -> m.getWeight() > 2000));
    }
}
