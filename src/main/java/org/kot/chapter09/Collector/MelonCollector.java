package org.kot.chapter09.Collector;

import org.kot.chapter05.MutableMelon;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;

public class MelonCollector implements Collector<MutableMelon, Map<Boolean, List<MutableMelon>>, Map<Boolean, List<MutableMelon>>> {

    @Override
    public Supplier<Map<Boolean, List<MutableMelon>>> supplier() {
        return () -> {
            return new HashMap<Boolean, List<MutableMelon>>() {
                {
                    put(true, new ArrayList<>());
                    put(false, new ArrayList<>());
                }
            };
        };
    }

    @Override
    public BiConsumer<Map<Boolean, List<MutableMelon>>, MutableMelon> accumulator() {
        return (var acc, var melon) -> {
            acc.get(melon.getWeight() > 2000).add(melon);
        };
    }

    @Override
    public BinaryOperator<Map<Boolean, List<MutableMelon>>> combiner() {
        return (var map, var addMap) -> {
            map.get(true).addAll(addMap.get(true));
            map.get(false).addAll(addMap.get(false));
            return map;
        };
    }

    @Override
    public Function<Map<Boolean, List<MutableMelon>>, Map<Boolean, List<MutableMelon>>> finisher() {
        return Function.identity();
    }

    @Override
    public Set<Characteristics> characteristics() {
        return Set.of(Characteristics.IDENTITY_FINISH, Characteristics.CONCURRENT);
    }
}
