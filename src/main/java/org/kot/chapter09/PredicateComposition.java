package org.kot.chapter09;

import org.kot.chapter05.MutableMelon;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import java.util.List;

public class PredicateComposition {
    public static void main(String[] args) {
        List<MutableMelon> melons = List.of(new MutableMelon("Gac", 2000),
                new MutableMelon("Hemi", 1600),
                new MutableMelon("Cantaloupe", 3000),
                new MutableMelon("Gac", 2200),
                new MutableMelon("Horned", 1500),
                new MutableMelon("Apollo", 2000),
                new MutableMelon("Horned", 1700));

        Predicate<MutableMelon> p2000 = m -> m.getWeight() > 2000;
        Predicate<MutableMelon> p2000GacApollo = p2000.and(m -> m.getType().equals("Gac"))
                .or(m -> m.getType().equals("Apollo"));
        Predicate<MutableMelon> restOf = p2000GacApollo.negate();
        Predicate<MutableMelon> pNot2000 = Predicate.not(p2000);

        /*
        COMPARATOR COMPOSITION
         */
        Comparator<MutableMelon> byWeight = Comparator.comparing(MutableMelon::getWeight);
        List<MutableMelon> sortedMelonsByWeight = melons.stream()
                .sorted(byWeight)
                .toList();

        Comparator<MutableMelon> byTypeDesc = Comparator.comparing(MutableMelon::getType).reversed();
        List<MutableMelon> sortedMelonsByType = melons.stream()
                .sorted(byTypeDesc)
                .toList();

        //mix comparators
        Comparator<MutableMelon> byTypeAndWeight = Comparator.comparing(MutableMelon::getWeight)
                .thenComparing(MutableMelon::getType);
        List<MutableMelon> sortedByTypeAndWeight = melons.stream()
                .sorted(byTypeAndWeight)
                .toList();

        //comparing ignoring case
        Comparator<MutableMelon> byTypeAndWeightIgnoreCase = Comparator.comparing(MutableMelon::getWeight)
                .thenComparing(MutableMelon::getType, String.CASE_INSENSITIVE_ORDER);
        List<MutableMelon> sortedByTypeAndWeightIgnoreCase = melons.stream()
                .sorted(byTypeAndWeightIgnoreCase)
                .toList();

        /*
        FUNCTION COMPOSITION
         */

        Function<Double, Double> f = x -> x * 2;
        Function<Double, Double> g = x -> Math.pow(x, 2);
        Function<Double, Double> gf = f.andThen(g); //(4 * 2 = 8) pow 2 = 64
        Function<Double, Double> fg = g.andThen(f); //(4 pow 2 = 16) * 2 = 32

        System.out.println("result fg: " + fg.apply(4d));
        System.out.println("result gf: " + gf.apply(4d));
    }
}
