package org.kot.chapter09;

import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;

public class MethodReference {
    public static void main(String[] args) {
        List<MelonMethodReference> melons = Arrays.asList(
                new MelonMethodReference("Crenshaw", 1200),
                new MelonMethodReference("Gac", 3000),
                new MelonMethodReference("Hemi", 2600),
                new MelonMethodReference("Hemi", 1600)
        );

        melons.forEach(MelonMethodReference::growing100g);
        MelonComparator mc = new MelonComparator();
        List<MelonMethodReference> sorted = melons.stream()
                .sorted(mc)
                .toList();

        List<Integer> sorted2 = melons.stream()
                .map(MelonMethodReference::getWeight)
                .sorted(Integer::compare)
                .toList();

        BiFunction<String, Integer, MelonMethodReference> melonFactory = MelonMethodReference::new;
        MelonMethodReference hemi1300 = melonFactory.apply("Hemi", 1300);
    }
}
