package org.kot.chapter09.MapStream;

import org.kot.chapter05.MutableMelon;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class StreamHelpers {
    public static void main(String[] args) {
        List<MutableMelon> melons = List.of(new MutableMelon("Gac", 2000),
                new MutableMelon("Hemi", 1600),
                new MutableMelon("Gac", 3000),
                new MutableMelon("Apollo", 2000),
                new MutableMelon("Horned", 1700));

        MutableMelon[][] melonsArray = {
        {new MutableMelon("Gac", 2000), new MutableMelon("Hemi", 1600)},
        {new MutableMelon("Gac", 2000), new MutableMelon("Apollo", 2000)},
        {new MutableMelon("Horned", 1700), new MutableMelon("Hemi", 1600)}
    };

        //get melon names
        List<String> melonNames = melons.stream()
                .map(MutableMelon::getType)
                .toList();

        //get melon weight
        List<Integer> melonWeight = melons.stream()
                .map(MutableMelon::getWeight)
                .toList();

        //set in the stream
        List<MutableMelon> melonWeightReduce = melons.stream()
                .map(m -> {
                    m.setWeight(m.getWeight() - 500);
                    return m;
                })
                .toList();

        //WRONG approach - peek is for look but don't change
        List<MutableMelon> melonWeightReducePeek = melons.stream()
                .peek(m -> m.setWeight(m.getWeight() - 500))
                .toList();


        /*
        Stream.flatMap()
         */
        Stream<MutableMelon[]> streamOfMutableMelonsArray = Arrays.stream(melonsArray);
        List<MutableMelon> uniqMelons = streamOfMutableMelonsArray
                .flatMap(Arrays::stream)
                .distinct()
                .toList();
        System.out.println(uniqMelons);
    }
}
