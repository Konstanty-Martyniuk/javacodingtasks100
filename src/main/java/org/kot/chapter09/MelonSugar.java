package org.kot.chapter09;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;
@AllArgsConstructor
@Getter
public class MelonSugar {
    enum Sugar {
        LOW, MEDIUM, HIGH, UNKNOWN
    }

    private final String type;
    private final int weight;
    private final Sugar sugar;

    public MelonSugar(String type, int weight) {
        this.type = type;
        this.weight = weight;
        this.sugar = Sugar.UNKNOWN;
    }

    @Override
    public String toString() {
        return type + "(" + weight + "g)" + (sugar != Sugar.UNKNOWN ? " " + sugar : "");
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.type);
        hash = 53 * hash + this.weight;
        hash = 53 * hash + Objects.hashCode(this.sugar);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MelonSugar other = (MelonSugar) obj;
        if (this.weight != other.weight) {
            return false;
        }
        if (!Objects.equals(this.type, other.type)) {
            return false;
        }
        return Objects.equals(this.sugar, other.sugar);
    }

}
