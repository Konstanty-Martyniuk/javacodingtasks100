package org.kot.chapter09;

import org.kot.chapter05.MutableMelon;
import org.kot.chapter08.TriFunction;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;
import static org.kot.chapter09.MelonSugar.Sugar.*;

public class SummarizationCollectors {
    public static void main(String[] args) {
        List<MutableMelon> melons = List.of(new MutableMelon("Gac", 2000),
                new MutableMelon("Hemi", 1600),
                new MutableMelon("Cantaloupe", 3000),
                new MutableMelon("Gac", 2200),
                new MutableMelon("Horned", 1500),
                new MutableMelon("Apollo", 2000),
                new MutableMelon("Horned", 1700));
        /*
        REDUCING
         */
        int sumWeightsGrams = melons.stream()
                .collect(Collectors.summingInt(MutableMelon::getWeight));

        int sumWeightsGrams2 = melons.stream()
                .mapToInt(MutableMelon::getWeight)
                .sum();

        double sumWeightsKg = melons.stream()
                .collect(Collectors.reducing(0.0,
                        m -> (double) m.getWeight() / 1000.0d, (m1, m2) -> m1 + m2));

        double sumWeightsKg2 = melons.stream()
                .map(m -> (double) m.getWeight() / 1000.0d)
                .reduce(0.0, Double::sum);

        /*
        AVERAGE
         */
        //average weight
        double averageWeight = melons.stream()
                .collect(Collectors.averagingDouble(MutableMelon::getWeight));

        /*
        COUNT
         */
        String str = "Lorem Ipsum is simply dummy text ...";
        long numberOfWords = Stream.of(str)
                .map(s -> s.split("\\s+"))
                .flatMap(Arrays::stream)
                .filter(w -> !w.trim().isEmpty())
                .count();

        long nrOfMelons = melons.stream()
                .filter(m -> m.getWeight() > 1500)
                .count();

        long nrOfMelons2 = melons.stream()
                .filter(m -> m.getWeight() > 1500)
                .collect(Collectors.counting());

        //min and max
        Comparator<MutableMelon> byWeight = Comparator.comparing(MutableMelon::getWeight);
        MutableMelon heaviestMelon = melons.stream()
                .collect(Collectors.maxBy(byWeight))
                .orElseThrow();
        //groupingBy but can contain duplicates
        Map<String, List<MutableMelon>> byTypeInList = melons.stream()
                .collect(groupingBy(MutableMelon::getType));

        //without duplicates
        Map<String, Set<MutableMelon>> byTypeInSet = melons.stream()
                .collect(groupingBy(MutableMelon::getType, toSet()));

        Map<String, List<MutableMelon>> byTypeInListDist = melons.stream()
                .distinct()
                .collect(groupingBy(MutableMelon::getType));

        Map<Integer, Set<MutableMelon>> byWeightSorted = melons.stream()
                .collect(groupingBy(MutableMelon::getWeight, TreeMap::new, toSet()));
        /*
        RESULT
        {
            2100 = [Gac (1200 g), Crenshaw(1200 g)],
            1600 = [Hemi(1600 g)],
            1700 = [Horned(1700 g)],
            2600 = [Hemi(2600 g), Apollo(2600 g)],
            3000 = [Gac(3000 g)]
        }
         */

        //List of 100 melons divide in 10 lists
        List<Integer> allWeights = new ArrayList<>(100);
        final AtomicInteger count = new AtomicInteger();
        Collection<List<Integer>> chunkWeights = allWeights.stream()
                .collect(Collectors.groupingBy(c -> count.getAndIncrement() / 10))
                .values();

        //convert from Melon to Melon Type
        Map<Integer, Set<String>> byWeightInSetOrdered = melons.stream()
                .collect(groupingBy(MutableMelon::getWeight, TreeMap::new,
                        mapping(MutableMelon::getType, toSet())));

        /*
        RESULT
        {
            1200 = [Crenshaw, Gac],
            1600 = [Hemi],
            1700 = [Horned],
            2600 = [Apollo, Hemi],
            3000 = [Gac]
        }
         */

        //count melons
        Map<String, Long> typesCount = melons.stream()
                .collect(groupingBy(MutableMelon::getType, counting()));

        //count by weight
        Map<Integer, Long> weightCount = melons.stream()
                .collect(groupingBy(MutableMelon::getWeight, counting()));
        /*
        OPTIONAL!!!!!
         */
        //collect min melon by type
        Map<String, Optional<MutableMelon>> minMelonByType = melons.stream()
                .collect(groupingBy(MutableMelon::getType, minBy(Comparator.comparingInt(MutableMelon::getWeight))));

        //fix previous
        Map<String, Integer> minMelonByTypeNice = melons.stream()
                .collect(groupingBy(MutableMelon::getType, collectingAndThen(
                        minBy(Comparator.comparingInt(MutableMelon::getWeight)), m -> m.orElseThrow().getWeight())));

        //group Melons by type in Array
        Map<String, MutableMelon[]> byTypeArray = melons.stream()
                .collect(groupingBy(MutableMelon::getType, collectingAndThen(
                        toList(), l -> l.toArray(MutableMelon[]::new))));

        /*
        MULTI LVL GROUPING
         */

        List<MelonSugar> melonsSugar = Arrays.asList(
                new MelonSugar("Crenshaw", 1200, HIGH),
                new MelonSugar("Gac", 3000, LOW),
                new MelonSugar("Hemi", 2600, HIGH),
                new MelonSugar("Hemi", 1600),
                new MelonSugar("Gac", 1200, LOW),
                new MelonSugar("Cantaloupe", 2600, MEDIUM),
                new MelonSugar("Cantaloupe", 3600, MEDIUM),
                new MelonSugar("Apollo", 2600, MEDIUM),
                new MelonSugar("Horned", 1200, HIGH),
                new MelonSugar("Gac", 3000, LOW),
                new MelonSugar("Hemi", 2600, HIGH));

        //sort by sugar and weight
        Map<MelonSugar.Sugar, Map<Integer, Set<String>>> bySugarAndWeight = melonsSugar.stream()
                .collect(groupingBy(MelonSugar::getSugar,
                        groupingBy(MelonSugar::getWeight, TreeMap::new, mapping(MelonSugar::getType, toSet()))));

        /*
        RESULT
        {
            MEDIUM = {
                2600 = [Apollo, Cantaloupe], 3600 = [Cantaloupe]
        },
            HIGH = (
                1200 = [Crenshaw, Horned], 2600 = [Hemi]
        },
            UNKNOWN = {
                1600 = [Hemi]
        },
            LOW = {
                1200 = [Gac], 3000 = [Gac]
        }
        }
         */
    }
}
