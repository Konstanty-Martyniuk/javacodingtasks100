package org.kot.chapter09.TestHighOrderFunctionsAndLambdas;

@FunctionalInterface
public interface Replacer<String> {
    String replace(String s);
}
