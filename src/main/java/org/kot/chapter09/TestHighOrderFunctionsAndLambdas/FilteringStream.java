package org.kot.chapter09.TestHighOrderFunctionsAndLambdas;

import java.util.Arrays;
import java.util.List;

public class FilteringStream {
    public static void main(String[] args) {
        List<Integer> ints = Arrays.asList(1, 2, -4, 0, 2, 0, -1, 14, 0, -1);
        List<Integer> result = ints.stream()
                .filter(i -> i != 0)
                .toList();

        List<Integer> fancyResult = ints.stream()
                .filter(i -> i != 0)
                .distinct()
                .skip(1)
                .limit(2)
                .sorted()
                .toList();

        System.out.println(result);
        System.out.println(fancyResult);


        //Bad solution
        List<Integer> badResult = ints.stream()
                .filter(value -> value > 0 && value < 10 && value % 2 == 0)
                .toList();

        //Good solution
        List<Integer> goodSolution = ints.stream()
                .filter(FilteringStream::evenBetween0and10)
                .toList();

        System.out.println("good solution: " + goodSolution);

    }

    private static boolean evenBetween0and10(int value) {
        return value > 0 && value < 10 && value % 2 == 0;
    }
}
