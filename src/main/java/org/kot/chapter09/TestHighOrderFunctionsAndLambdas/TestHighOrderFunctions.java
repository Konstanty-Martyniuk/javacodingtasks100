package org.kot.chapter09.TestHighOrderFunctionsAndLambdas;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestHighOrderFunctions {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("anna", "bob", "mary");
        List<String> upperNames = names.stream()
                .map(s -> s.toUpperCase())
                .toList();

    }

    public static List<String> replace(List<String> list, Replacer<String> r) {
        List<String> result = new ArrayList<>();
        for (var s : list) {
            result.add(r.replace(s));
        }
        return result;
    }

    public static Function<String, String> reduceStrings(Function<String, String>... functions) {

        return Stream.of(functions)
                .reduce(Function.identity(), Function::andThen);
    }

    public static final Function<String, String> firstAndLastChar =
            s -> s.charAt(0) + String.valueOf(s.charAt(s.length() - 1));
    
    public List<String> rndStringFromString(List<String> strings) {
        return strings.stream()
                .map(TestHighOrderFunctions::getString)
                .toList();
    }

    public static String getString(String s) {
        Random random = new Random();
        int nr = random.nextInt(s.length());
        return String.valueOf(s.charAt(nr));
    }
}
