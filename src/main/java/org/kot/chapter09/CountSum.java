package org.kot.chapter09;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class CountSum {
    private final Long count;
    private final int sum;
}
