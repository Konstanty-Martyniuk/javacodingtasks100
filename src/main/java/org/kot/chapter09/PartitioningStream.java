package org.kot.chapter09;

import org.kot.chapter05.MutableMelon;

import java.util.*;

import static java.util.stream.Collectors.*;

public class PartitioningStream {
    public static void main(String[] args) {
        List<MutableMelon> melons = List.of(new MutableMelon("Gac", 2000),
                new MutableMelon("Hemi", 1600),
                new MutableMelon("Cantaloupe", 3000),
                new MutableMelon("Gac", 2200),
                new MutableMelon("Horned", 1500),
                new MutableMelon("Apollo", 2000),
                new MutableMelon("Horned", 1700));

        //partitioning by weight 2000
        Map<Boolean, List<MutableMelon>> byWeight = melons.stream()
                .collect(partitioningBy(m -> m.getWeight() > 2000));
        System.out.println(byWeight);
        /*
        RESULT
            {false=[MutableMelon(type=Gac, weight=2000), MutableMelon(type=Hemi, weight=1600), MutableMelon(type=Horned, weight=1500), MutableMelon(type=Apollo, weight=2000), MutableMelon(type=Horned, weight=1700)],
            true=[MutableMelon(type=Cantaloupe, weight=3000), MutableMelon(type=Gac, weight=2200)]}
         */

        //partitioning by weight 2000 uniq
        Map<Boolean, List<MutableMelon>> byWeightUniq = melons.stream()
                .distinct()
                .collect(partitioningBy(m -> m.getWeight() > 2000));

        Map<Boolean, Set<MutableMelon>> byWeightSet = melons.stream()
                .collect(partitioningBy(m -> m.getWeight() > 2000, toSet()));

        //count false/true
        Map<Boolean, Long> byWeightAndCount = melons.stream()
                .collect(partitioningBy(m -> m.getWeight() > 2000, counting()));
        System.out.println(byWeightAndCount); //{false=5, true=2}

        //by weight and only max
        Map<Boolean, MutableMelon> byWeightMax = melons.stream()
                .collect(partitioningBy(m -> m.getWeight() > 2000,
                        collectingAndThen(maxBy(Comparator.comparingInt(MutableMelon::getWeight)), Optional::get)));
    }
}
