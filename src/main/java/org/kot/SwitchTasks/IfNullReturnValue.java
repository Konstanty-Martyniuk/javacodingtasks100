package org.kot.SwitchTasks;

import java.util.Objects;

public class IfNullReturnValue {

}

class CarWithBasicParamsIfNull {
    public final String name;
    public final Color color;

    public CarWithBasicParamsIfNull(String name, Color color) {
        this.name = Objects.requireNonNullElse(name, "Noname car");
        this.color = Objects.requireNonNullElseGet(color, () -> new Color(0,0,0));
    }
}

class Color {
    private final int red;
    private final int green;
    private final int blue;

    public Color(int red, int green, int blue) {
        this.red = Objects.requireNonNullElse(red, 0);
        this.green = Objects.requireNonNullElse(green, 0);
        this.blue = Objects.requireNonNullElse(blue, 0);
    }
}
