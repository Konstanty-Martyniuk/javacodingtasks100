package org.kot.SwitchTasks;

import java.util.Objects;

public class CheckIndex {
    private static final int X_UPPER_BOUND = 11;
    private static final int Y_UPPER_BOUND = 16;
    private static final int N_UPPER_BOUND = 101;
    private final int n;
    private final int x;

    public CheckIndex(int n, int x) {
        this.x = Objects.checkIndex(x, X_UPPER_BOUND);
        this.n = Objects.checkIndex(n, N_UPPER_BOUND);
    }

    public int xMinusY(int y) {
        Objects.checkIndex(y, x);

        return x - y;
    }

    public static int oneMinusY(int y) {
        Objects.checkIndex(y, Y_UPPER_BOUND);
        return 1 - y;
    }

    public int yMinusX(int y, int n, int x) {
        Objects.checkFromToIndex(y, x, n);
        return y - x;
    }

}
