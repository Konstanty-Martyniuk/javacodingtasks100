package org.kot.SwitchTasks;

import java.util.Objects;
import java.util.function.Supplier;

public class MyExceptions {
    public static <T> T requireNonNullElseThrowIAE(T obj, String message) {
        if (Objects.isNull(obj)) {
            throw new IllegalArgumentException(message);
        }
        return obj;
    }

    public static <T> T requireNonNullElseThrowIAE(T obj, Supplier<String> messageSupplier) {
        if (Objects.isNull(obj)) {
            throw new IllegalArgumentException(messageSupplier == null ? null : messageSupplier.get());
        }
        return obj;
    }

    public static <T, X extends Throwable> T requireNonNullElseThrow
            (T obj, Supplier<? extends X> exceptionSupplier) throws X {
        if (!Objects.isNull(obj)) {
            return obj;
        } else {
            throw exceptionSupplier.get();
        }
    }
}
