package org.kot.SwitchTasks;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.rits.cloning.Cloner;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PointDeepCloning implements Serializable, Cloneable {
    private double x;
    private double y;
    private Radius radius;
}
