package org.kot.SwitchTasks;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Point implements Cloneable {
    private double x;
    private double y;

    public Point(Point anotherPoint) {
        this.x = anotherPoint.x;
        this.y = anotherPoint.y;
    }

    @Override
    public Point clone() throws CloneNotSupportedException {
        return (Point) super.clone();
    }

}
