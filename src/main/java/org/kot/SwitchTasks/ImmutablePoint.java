package org.kot.SwitchTasks;

import lombok.Getter;

public final class ImmutablePoint {
    @Getter
    private final double x;
    @Getter
    private final double y;
    private final Radius radius;

    public ImmutablePoint(double x, double y, Radius radius) {
        this.x = x;
        this.y = y;

        Radius clone = new Radius();
        clone.setStart(radius.getStart());
        clone.setEnd(radius.getEnd());
        this.radius = clone;
    }

    public Radius getRadius() {
        Radius clone = new Radius();
        clone.setStart(this.radius.getStart());
        clone.setEnd(this.radius.getEnd());
        return clone;
    }
}
