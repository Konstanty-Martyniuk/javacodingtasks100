package org.kot.SwitchTasks;

import java.util.*;

public class CheckNulls {
    List<Integer> numbers = Arrays.asList(1, 2, null, 4, null, 16, 7, null);

    public static List<Integer> evenIntegers(List<Integer> numbers) {
        if(Objects.isNull(numbers)) {
            return Collections.EMPTY_LIST;
        }

        List<Integer> evenNumbers = new ArrayList<>();

        for (Integer number : numbers) {
            if(Objects.nonNull(number) && number % 2 == 0) {
                evenNumbers.add(number);
            }
        }
        return evenNumbers;
    }

    public static int sumIntegers(List<Integer> numbers) {
        if(Objects.isNull(numbers)) {
            throw new IllegalArgumentException("List of numbers cannot be null");
        }

        return numbers.stream()
                .filter(Objects::nonNull)
                .mapToInt(Integer::intValue)
                .sum();
    }

    public static boolean integersContainsNulls(List<Integer> integers) {
        if(Objects.isNull(integers)) {
            return false;
        }
        return integers.stream()
                .anyMatch(Objects::isNull);
    }
}
