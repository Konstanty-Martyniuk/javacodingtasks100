package org.kot.SwitchTasks;

import java.util.Objects;
import java.util.function.Supplier;

public class CarWithNPE {
    public final String name;
    public final String color;

    public CarWithNPE(String name, String color) {
        if (Objects.isNull(name) || Objects.isNull(color)) {
            throw new NullPointerException("Name and color cannot be null");
        }
        this.name = name;
        this.color = color;
    }
}

class AnotherCarWithNPE {
    public final String name;
    public final String color;

    public AnotherCarWithNPE(String name, String color) {
        this.name = Objects.requireNonNull(name, "Name cannot be null");
        this.color = MyExceptions.requireNonNullElseThrow(name, () -> new UnsupportedOperationException("Color cannot be null"));
    }
}
