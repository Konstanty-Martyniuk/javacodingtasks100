package org.kot.SwitchTasks;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Objects;

@Getter
@Setter
@AllArgsConstructor
public class PlayerEqualsHash {
    private int id;
    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlayerEqualsHash that = (PlayerEqualsHash) o;
        return id == that.id && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }

    public static void main(String[] args) {
        PlayerEqualsHash player1 = new PlayerEqualsHash(1, "John");
        PlayerEqualsHash player2 = new PlayerEqualsHash(1, "John");

        System.out.println(player1.equals(player2)); //false before overriding equals
        System.out.println(player1.hashCode()); //81628611
        System.out.println(player2.hashCode()); //1828972342
    }
}
