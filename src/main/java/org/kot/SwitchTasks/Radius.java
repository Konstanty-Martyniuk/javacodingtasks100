package org.kot.SwitchTasks;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Radius {
    private int start;
    private int end;

    @Override
    public String toString() {
        return "Radius{" +
                "start=" + start +
                ", end=" + end +
                '}';
    }
}
