package org.kot.SwitchTasks;


import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.util.Date;

public final class User {
    private final String nickname;
    private final String password;
    private final String firstName;
    private final String lastName;
    private final String email;
    private final Date created;

    private User(UserBuilder builder) {
        this.nickname = builder.nickname;
        this.password = builder.password;
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.email = builder.email;
        this.created = builder.created;
    }

    public static UserBuilder getBuilder(String nickname, String password) {
        return new User.UserBuilder(nickname, password);
    }
    public static final class UserBuilder {
        @NotNull
        @Size(min = 3, max = 20)
        private String nickname;
        @NotNull
        @Size(min = 6, max = 20)
        private String password;
        @Size(max = 20)
        private String firstName;
        @Size(max = 20)
        private String lastName;
        @Email
        private String email;
        private Date created;

        public UserBuilder(String nickname, String password) {
            this.nickname = nickname;
            this.password = password;
            this.created = new Date();
        }

        public UserBuilder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public UserBuilder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public UserBuilder email(String email) {
            this.email = email;
            return this;
        }

        public User build() {
            return new User(this);
        }

    }

}
