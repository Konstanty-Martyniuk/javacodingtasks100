package org.kot.chapter05;

import java.util.Iterator;
import java.util.LinkedList;

public class BreadthFirstSearch {
    static class Graph {
        private int v; //liczba wierzchołków
        private LinkedList<Integer>[] adjacencyList; //lista sąsiadów

        //Constructor
        public Graph(int v) {
            this.v = v;
            adjacencyList = new LinkedList[v];
            for (int i = 0; i < v; i++) {
                adjacencyList[i] = new LinkedList<>();
            }
        }

        // Function to add an edge from vertex 'source' to vertex 'destination' in the graph
        void addEdge(int source, int destination) {
            adjacencyList[source].add(destination);
        }

        // Function to perform BFS traversal starting from source
        void BFS(int start) {
            //Mark all vertices as not visited
            boolean visited[] = new boolean[v];
            //Create a queue for BFS
            LinkedList<Integer> queue = new LinkedList<>();
            //Mark current node as visited and add to queue it
            visited[start] = true;
            queue.add(start);

            while (!queue.isEmpty()) {
                // Dequeue a vertex from queue and print it
                start = queue.poll();
                System.out.print(start + " ");
                // Get all adjacent vertices of the dequeued vertex source.
                // If an adjacent vertex has not been visited, then mark it visited and enqueue it

                for (int next : adjacencyList[start]) {
                    if (!visited[next]) {
                        visited[next] = true;
                        queue.add(next);
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
        Graph g = new Graph(4);
        g.addEdge(0, 1);
        g.addEdge(0, 2);
        g.addEdge(1, 2);
        g.addEdge(2, 0);
        g.addEdge(2, 3);
        g.addEdge(3, 3);

        System.out.println("BFS starting from 2");
        g.BFS(2);
    }
}
