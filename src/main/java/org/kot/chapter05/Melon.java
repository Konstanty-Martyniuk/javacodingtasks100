package org.kot.chapter05;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Melon implements Comparable{
    private final String type;
    private final int weight;

    @Override
    public int compareTo(Object o) {
        Melon m = (Melon) o;
        return Integer.compare(this.getWeight(), m.getWeight());
    }
}
