package org.kot.chapter05;

import java.util.HashMap;
import java.util.Map;

public class MergeMap {
    public static void main(String[] args) {
        Map<Integer, Melon> melons1 = new HashMap<>();
        Map<Integer, Melon> melons2 = new HashMap<>();

        melons1.put(1, new Melon("Apollo", 3000));
        melons1.put(2, new Melon("Jade Dew", 3500));
        melons1.put(3, new Melon("Cantaloupe", 1500));
        melons2.put(3, new Melon("Apollo", 3000));
        melons2.put(4, new Melon("Jade Dew", 3500));
        melons2.put(5, new Melon("Cantaloupe", 1500));
    }
}
