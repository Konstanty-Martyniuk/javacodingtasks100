package org.kot.chapter05;

import java.util.Arrays;
import java.util.Comparator;

public class InsertionSort {
    public static void insertionSortPrimitive(int arr[]) {
        int arrLength = arr.length;
        for (int i = 1; i < arrLength; i++) {

            int key = arr[i];
            int j = i;

            while (j > 0 && arr[j - 1] > key) {
                arr[j] = arr[j - 1];
                j--;
            }
            arr[j] = key;
        }
        System.out.println("Insertion sort");
        System.out.println(Arrays.toString(arr));
    }

    public static <T> void insertionSortWithComparator(T arr[], Comparator<? super T> comparator) {
        int length = arr.length;
        for (int i = 1; i < length; ++i) {

            T key = arr[i];
            int j = i - 1;

            while (j >= 0 && comparator.compare(arr[j], key) > 0) {
                arr[j + 1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = key;
        }
    }
}
