package org.kot.chapter05;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class CopyHashMap {
    public static void main(String[] args) {

    }

    public static <K, V>HashMap<K, V> justCopy(Map<K,V> map) {
        return new HashMap<>(map);
    }

    public static <K, V>HashMap<K, V> shallowCopy(Map<K,V> map) {
        HashMap<K,V> copy = new HashMap<>();
        copy.putAll(map);

        return copy;
    }

    public static <K, V>HashMap<K, V> shallowCopyWithStream(Map<K,V> map) {
        Set<Map.Entry<K,V>> entries = map.entrySet();
        return (HashMap<K, V>) entries.stream()
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }
}
