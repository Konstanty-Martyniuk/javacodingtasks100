package org.kot.chapter05;

import java.util.Arrays;
import java.util.Collections;
import java.util.stream.IntStream;

public class ReverseArray {
    int[] integers = {1, 2, 3, 4};
    static Melon[] melons = {new Melon ("Horned", 1500), new Melon ("Gac", 2200),
            new Melon("Gac", 2100)};

    public static void reverseArr(int[] arr) {
        for (int leftHead = 0, rightHead = arr.length - 1; leftHead < rightHead; leftHead++, rightHead--) {
            int temp = arr[leftHead];
            arr[leftHead] = arr[rightHead];
            arr[rightHead] = temp;
        }
    }

    //reverse with stream
    int[] reversedWithStream = IntStream
            .rangeClosed(1, integers.length)
            .map(i -> integers[integers.length - i])
            .toArray();

    //With objects
    public static <T> void reverseArr(T[] arr) {
        for (int leftHead = 0, rightHead = arr.length - 1; leftHead < rightHead; leftHead++, rightHead--) {
            T temp = arr[leftHead];
            arr[leftHead] = arr[rightHead];
            arr[rightHead] = temp;
        }
    }

    Melon[] reversed = IntStream.rangeClosed(1, melons.length)
            .mapToObj(i -> melons[melons.length - i])
            .toArray(Melon[]::new);

    public static void main(String[] args) {
        Collections.reverse(Arrays.asList(melons));
    }
}
