package org.kot.chapter05;

import java.util.Arrays;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamFromArray {
    String[] stringArr = {"One", "Two", "Three", "Four", "Five"};

    //Arrays.stream()
    Stream<String> stringStream = Arrays.stream(stringArr);
    Stream<String> partialStringStream = Arrays.stream(stringArr, 0, 2);

    //Arrays.asList()
    Stream<String> streamAsList = Arrays.asList(stringArr).stream();
    Stream<String> streamSubAsList = Arrays.asList(stringArr).subList(0, 2).stream();

    //Stream.of()
    Stream<String> streamOf = Stream.of(stringArr);
    Stream<String> streamOfValues = Stream.of("One", "Two", "Three", "Four", "Five");

    //Stream to Array
    String[] arrayOfStrings = streamOfValues.toArray(String[]::new);
    int[] integers = {1, 2, 3, 4};
    IntStream intStream = Arrays.stream(integers);
    IntStream intStreamOf = IntStream.of(integers);

    IntStream intStreamExclusive = IntStream.range(0, integers.length);
    IntStream intStreamInclusive = IntStream.rangeClosed(0, integers.length);

    int[] intArray = intStream.toArray();
}
