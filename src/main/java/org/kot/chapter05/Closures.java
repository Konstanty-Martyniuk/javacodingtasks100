package org.kot.chapter05;

import java.util.Comparator;
import java.util.Map;

import static java.util.Map.entry;

public class Closures {
    public static <T> Map.Entry<T, T> arrayOfT(T[] array, Comparator<? super T> comparator) {
        T min = array[0];
        T max = array[0];

        for (T element : array) {
            if (comparator.compare(min, element) > 0) {
                min = element;
            } else if (comparator.compare(max, element) < 0) {
                max = element;
            }
        }
        return entry(min, max);
    }
}
