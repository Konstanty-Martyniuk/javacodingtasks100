package org.kot.chapter05;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class UnmodifiableCollections {
    private static final List<Integer> LIST = Collections.unmodifiableList(Arrays.asList(1,2,3,4,5,6));
    private static final List<Integer> LISTOF = List.of(1, 2, 3, 4, 5, 6);


    public static void main(String[] args) {

    }

}
