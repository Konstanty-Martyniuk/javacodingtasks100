package org.kot.chapter05;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public final class ImmutableMelon {
    private final String type;
    private final int weight;
}
