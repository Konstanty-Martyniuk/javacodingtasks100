package org.kot.chapter05;

import java.util.*;
import java.util.stream.IntStream;

import static java.util.Map.entry;

public class Main {
    //Unmodifiable array
    private final MutableMelon mutableMelon1 = new MutableMelon("Crenshaw", 2000);
    private final MutableMelon mutableMelon2 = new MutableMelon("Gac", 1200);

    private final List<MutableMelon> mutableMelonList = Collections.unmodifiableList(Arrays.asList(mutableMelon1, mutableMelon2));
    private final List<MutableMelon> mutableMelonListOf = List.of(mutableMelon1, mutableMelon2);

    //Immutable array
    private static final ImmutableMelon immutableMelon1 = new ImmutableMelon("Crenshaw", 2000);
    private static final ImmutableMelon immutableMelon2 = new ImmutableMelon("Gac", 1200);

    private static final List<ImmutableMelon> immutableMelons = List.of(immutableMelon1, immutableMelon2);

    //Unmodifiable map
    Map<Integer, MutableMelon> mapOfSingleMutableMelon = Collections.singletonMap(1, new MutableMelon("Gac", 1200));
    Map<Integer, MutableMelon> mutableMelonMap = Map.ofEntries(
            entry(1, new MutableMelon("Appollo", 3000)),
            entry(2, new MutableMelon("Jade Dew", 3500)),
            entry(3, new MutableMelon("Cantaloupe", 1500))
    );


    //Immutable map
    //Empty map
    Map<Integer, MutableMelon> emptyMap = Collections.emptyMap();
    //Singleton
    Map<Integer, ImmutableMelon> mapOfSingleImmutableMelon = Collections.singletonMap(1, new ImmutableMelon("Gac", 1200));
    //Map of entities
    Map<Integer, ImmutableMelon> immutableMelonMap = Map.ofEntries(
            entry(1, new ImmutableMelon("Apollo", 3000)),
            entry(2, new ImmutableMelon("Jade Dew", 3500)),
            entry(3, new ImmutableMelon("Cantaloupe", 1500))
    );
    //Map.copyOf
    Map<Integer, ImmutableMelon> copyOfImmutableMapOfMelon = Map.copyOf(immutableMelonMap);

    public static void main(String[] args) {
        Melon[] melons = new Melon[] { new Melon("Sweet", 25), new Melon("Big", 50) };

        Arrays.sort(melons, new Comparator<Melon>() {
            @Override
            public int compare(Melon melon1, Melon melon2) {
                return Integer.compare(melon1.getWeight(), melon2.getWeight());
            }
        });
        //ASC
        Arrays.sort(melons, (melon1, melon2) -> Integer.compare(melon1.getWeight(), melon2.getWeight()));
        //DESC
        Arrays.sort(melons, (melon2, melon1) -> Integer.compare(melon2.getWeight(), melon1.getWeight()));

        Arrays.sort(melons, Comparator.comparingInt(Melon::getWeight));

        //BubbleSorting.bubbleSort(new int[] {11, 3, 14, 16, 7});
        BubbleSorting.bubbleSortWhile(new int[] {11, 3, 14, 16, 7});

        Comparator<Melon> byType = Comparator.comparing(Melon::getType);
        Comparator<Melon> byTypeDesc = Comparator.comparing(Melon::getType).reversed();

        Arrays.sort(melons, byType);
        InsertionSort.insertionSortPrimitive(new int[] {11, 3, 14, 16, 7, 55, 1, 9, 13});
        SelectionSort.selectionSort(new int[] {11, 3, 14, 16, 7, 55, 1, 9, 13});

        NextGreaterElement.nextGreaterElement(new int[] {11, 3, 14, 16, 7, 55, 1, 9, 13});
    }

    //Contains element
    public static boolean containsElement(int[] arr, int key) {
        return Arrays.stream(arr).anyMatch(e -> e == key);
    }

    public static boolean containsElementAsList(int[] arr, int key) {
        return Collections.singletonList(arr).contains(key);
    }

    //Check first index
    public static int findIndexOfElement(int[] arr, int toFind) {
        return IntStream.range(0, arr.length)
                .filter(i -> arr[i] == toFind)
                .findFirst()
                .orElse(-1);
    }

    //equals
    public static <T> int findIndexOfElementObject(T[] arr, T toFind) {
        for (int i = 0; i < arr.length; i++) {
            if (toFind.equals(arr[i])) {
                return i;
            }
        }
        return -1;
    }

    public static <T> int findIndexOfElementObjectStream(T[] arr, T toFind, Comparator<? super T> comparator) {
        return IntStream.range(0, arr.length)
                .filter(i -> comparator.compare(toFind, arr[i]) == 0)
                .findFirst()
                .orElse(-1);
    }
}
