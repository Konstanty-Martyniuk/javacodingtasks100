package org.kot.chapter05;

import java.util.Arrays;
import java.util.Stack;

public class NextGreaterElement {
    public static void nextGreaterElement(int[] arr) {
        int n = arr.length;
        int[] result = new int[n];
        Arrays.fill(result, -1);
        Stack<Integer> stack = new Stack<>();

        for (int i = 0; i < n; i++) {
            while (!stack.isEmpty() && arr[i] > arr[stack.peek()]) {
                result[stack.pop()] = arr[i];
            }
            stack.push(i);
        }
        System.out.println(Arrays.toString(result));
    }
}
