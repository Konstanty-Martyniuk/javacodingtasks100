package org.kot.chapter05;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CompareMapArray {
    public static void main(String[] args) {
        Map<Integer, Melon> melons1Map = new HashMap<>();
        Map<Integer, Melon> melons2Map = new HashMap<>() ;
        melons1Map.put(1, new Melon("Apollo", 3000));
        melons1Map.put(2, new Melon("Jade Dew", 3500));
        melons1Map.put(3, new Melon("Cantaloupe", 1500));
        melons2Map.put(1, new Melon("Apollo", 3000));
        melons2Map.put(2, new Melon("Jade Dew", 3500));
        melons2Map.put(3, new Melon("Cantaloupe", 1500));

        //Works
        boolean equalsMap1toMap2 = melons1Map.equals(melons2Map);

        Melon[] melons1Array = {new Melon("Apollo", 3000), new Melon("Jade Dew", 3500),
                new Melon("Cantaloupe", 1500)
        };

        Melon[] melons2Array = {new Melon("Apollo", 3000), new Melon("Jade Dew", 3500),
                new Melon("Cantaloupe", 1500)
        };

        Map<Integer, Melon[]> melons1ArrayMap = new HashMap<>();
        melons1ArrayMap.put(1, melons1Array);
        Map<Integer, Melon[]> melons2ArrayMap = new HashMap<>();
        melons2ArrayMap.put(1, melons2Array);

        //way to compare is to use help method
    }

    public static <A, B> boolean equalsWithArrays (Map<A, B[]> first, Map<A, B[]> second) {
        if (first.size() != second.size()) {
            return false;
        }

        return first.entrySet()
                .stream()
                .allMatch(e -> Arrays.equals(e.getValue(), second.get(e.getKey())));
    }
}
