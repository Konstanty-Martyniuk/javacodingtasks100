package org.kot.chapter05;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;

/*
    SORT METHODS
 */
public class Maps {
   public static <K, V> TreeMap<K, V> sortByKeyTreeMap(Map<K, V> map) {
      return new TreeMap<>(map);
   }

   public static <K, V> Map<K, V> sortByKeyStream (Map<K, V> map, Comparator<? super K> comparator) {
      return map.entrySet().stream()
              .sorted(Map.Entry.comparingByKey(comparator))
              .collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (v1, v2) -> v1, LinkedHashMap::new));
   }

   public static <K, V> Map<K, V> sortByValueStream (Map<K, V> map, Comparator<? super V> comparator) {
      return map.entrySet().stream()
              .sorted(Map.Entry.comparingByValue(comparator))
              .collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (v1, v2) -> v1, LinkedHashMap::new));
   }

   public static <K extends Comparable, V> List<K> sortedByKeyList(Map<K ,V> map) {
      List<K> list = new ArrayList<>(map.keySet());
      Collections.sort(list);

      return list;
   }

   public static <K, V extends Comparable> List<V> sortedByValueList(Map<K, V> map) {
      List<V> list = new ArrayList<>(map.values());
      Collections.sort(list);

      return list;
   }

   /*
   MERGE METHODS
    */
   public static <K, V> Map<K, V> mergeMaps(Map<K, V> map1, Map<K, V> map2) {
      Map<K, V> map = new HashMap<>(map1);

      map2.forEach((k, v) -> map.merge(k, v , (v1, v2) -> v2));

      return map;
   }

   public static <K, V> Map<K, V> mergeMapWithStream(Map<K, V> map1, Map<K, V> map2) {
      Stream<Map.Entry<K, V>> combined = Stream.concat(map1.entrySet().stream(), map2.entrySet().stream());

       return combined.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (v1, v2) -> v2));
   }
}
