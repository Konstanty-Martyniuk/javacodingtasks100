package org.kot.chapter05;

import java.util.Arrays;
import java.util.Comparator;

public class ArraysEqual {
    int[] integers1 = {3, 4, 5, 6, 1, 5};
    int[] integers2 = {3, 4, 5, 6, 1, 5};
    int[] integers3 = {3, 4, 5, 6, 1, 3};

    Melon[] melons1 = {new Melon("Horned", 1500), new Melon("Gac", 1000)};
    Melon [] melons2 = {new Melon("Horned", 1500), new Melon("Gac", 1000)};
    Melon[] melons3 = {new Melon("Hami", 1500), new Melon("Gac", 1000)};

    Comparator<Melon> byType = Comparator.comparing(Melon::getType);

    boolean i12 = Arrays.equals(integers1, integers2);

    //Check if intervals are equal
    boolean i12Intervals = Arrays.equals(integers1, 1, 4, integers3, 1, 4);
    boolean melons12ByIntervalAndType = Arrays.equals(melons1, 1, 2, melons2, 1, 2, byType);

    //Check arrays mismatch
    int mismatch12 = Arrays.mismatch(integers1, integers2); //-1
    int mismatch13 = Arrays.mismatch(integers1, integers3); //5

    //lexicographical comparison
    int lexComp12 = Arrays.compare(integers1, integers3);
    int lexCompMelons = Arrays.compare(melons1, melons3);
    int lexCompMelonsWithComparator = Arrays.compare(melons1, melons3, byType);
}
