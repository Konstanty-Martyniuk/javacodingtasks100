package org.kot.chapter05;

import java.util.Arrays;

public class SelectionSort {

    public static void selectionSort(int[] arr) {
        for (int step = 0; step < arr.length; step++) {
            int index = min(arr, step);
            int temp = arr[step];
            arr[step] = arr[index];
            arr[index] = temp;
        }
        System.out.println("Selection sort");
        System.out.println(Arrays.toString(arr));
    }


    public static int min(int[] arr, int start) {
        int minIndex = start;
        int minValue = arr[start];

        for (int i = start + 1; i < arr.length; i++) {
            if (arr[i] < minValue) {
                minValue = arr[i];
                minIndex = i;
            }
        }
        return minIndex;
    }
}
