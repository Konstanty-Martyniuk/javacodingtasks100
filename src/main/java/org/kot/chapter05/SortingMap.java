package org.kot.chapter05;

import java.util.*;

public class SortingMap {
    public static void main(String[] args) {
        Map<String, Melon> melons = new HashMap<>();
        melons.put("delicious", new Melon("Apollo", 3000));
        melons.put("refreshing", new Melon("Jade Dew", 3500));
        melons.put("famous", new Melon("Cantaloupe", 1500));

        //Sorting via TreeMap
        TreeMap<String, Melon> sortedMelons = Maps.sortByKeyTreeMap(melons);

        //Sorting via Stream and Comparator
        Comparator<String> byInt = Comparator.naturalOrder();
        Map<String, Melon> sortedMap = Maps.sortByKeyStream(melons, byInt);

        Comparator<Melon> byWeight = Comparator.comparing(Melon::getWeight);
        Map<String, Melon> sortedByWeight = Maps.sortByValueStream(melons, byWeight);

        List<String> sortedKeys = Maps.sortedByKeyList(melons);
        List<Melon> sortedValues = Maps.sortedByValueList(melons);

        //In case of duplicates
        SortedSet<String> sortedKeysSet = new TreeSet<>(melons.keySet());
        SortedSet<Melon> sortedValuesSet = new TreeSet<>(melons.values());
    }
}
