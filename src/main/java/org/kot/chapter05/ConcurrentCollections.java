package org.kot.chapter05;

import java.util.Deque;
import java.util.List;
import java.util.Set;
import java.util.concurrent.*;

public class ConcurrentCollections {
    public static void main(String[] args) {
        //ArrayList == CopyOnWriteArrayList, use when read often, write not
        //LinkedList == Vector
        List<Integer> listInteger = new CopyOnWriteArrayList<>();

        //Set == CopyOnWriteArraySet, use when read often, write not
        //HashSet == ConcurrentSkipListSet
        //NavigableSet == ConcurrentSkipListSet
        Set<Integer> setInteger = new CopyOnWriteArraySet<>();

        //HashMap == ConcurrentHashMap - best performance
        //TreeMap == ConcurrentSkipListMap
        //NavigableMap == ConcurrentSkipListMap
        //LinkedHashMap == HashTable
        ConcurrentMap<Integer, Integer> mapOfIntegers = new ConcurrentHashMap<>();
        ConcurrentNavigableMap<Integer, Integer> mapNavigable = new ConcurrentSkipListMap<>();

        //Concurrent arrays:
        /*
       • ArrayBlockingQueue (ograniczona)
       • ConcurrentLinkedQueue (nieograniczona)
       • ConcurrentLinkedDeque (nieograniczona)
       • LinkedBlockingQueue(opcjonalnie ograniczona)
       • LinkedBlockingDeque(opcjonalnie ograniczona)
       • LinkedTransferQueue
       • PriorityBlockingQueue
       • SynchronousQueue
       • DelayQueue
       • Stack
         */
        BlockingQueue<Integer> queueABQ = new ArrayBlockingQueue<>(10);
        Deque<Integer> queueCLD = new ConcurrentLinkedDeque<>();
        BlockingQueue<Integer> queuePBQ = new PriorityBlockingQueue<>();
    }


}
