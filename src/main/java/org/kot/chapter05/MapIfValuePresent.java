package org.kot.chapter05;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;

public class MapIfValuePresent {
    //compute(), computeIfAbsent(), computeIfPresent() and merge()

    public static void main(String[] args) throws UnknownHostException {
        //computeIfPresent()
        Map<String, String> map =  new HashMap<>();
        map.put("postgresql", "127.0.0.1:5432");
        map.put("mysql", "192.168.0.50:3306");
        map.put("cassandra", "192.168.1.5:9042");

        BiFunction<String, String, String> jdbcUrl = (k, v) -> "jdbc:" + k + "://" + v + "/customers_db";
        String mySqlJdbcUrl = map.computeIfPresent("mysql", jdbcUrl);

        //computeIfAbsent()
        String address = InetAddress.getLocalHost().getHostAddress();
        Function<String, String> jdbcUrlCIA = k -> k + "://" + address + "/customer_db";

        String mongoDbJdbcUrl = map.computeIfAbsent("mongodb", jdbcUrlCIA);

        //compute()
        BiFunction<String, String, String> computeJdbcUrl = (k, v) -> "jdbc" + k + "://" + ((v == null) ? address : v)
                + "/customer_db";

        String derbyUrl = map.compute("derby", computeJdbcUrl);

        for (var entry : map.entrySet()) {
            System.out.println(entry.getKey() + ":" + entry.getValue());
        }
        System.out.println("++++++++++++++++++++");
        mergeMap();
    }

    public static void mergeMap() {
        //merge()
        Map<String, String> map =  new HashMap<>();
        map.put("postgresql", "9.6.1 ");
        map.put("mysql", "5.1 5.2 5.6 ");

        BiFunction<String, String, String> mergeJdbcUrl = String::concat;

        String mySqlVersion = map.merge("mysql", "9.0 ", mergeJdbcUrl);
        String derbyVersion = map.merge("derby", "10.11.1.1 ", mergeJdbcUrl);

        for (var entry : map.entrySet()) {
            System.out.println(entry.getKey() + ":" + entry.getValue());
        }
    }

    public static void putIfAbsentMap() {
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "postgresql");
        map.put(2, "mysql");
        map.put(3, null);

        String vl = map.putIfAbsent(1, "derby"); // postgresql
        String v2 = map.putIfAbsent(3, "derby"); // null
        String v3 = map.putIfAbsent(4, "cassandra"); // null
    }

    public static void remove() {
        Map<Integer, String> map = new HashMap<>();
        map.put(1, "postgresql");
        map.put(2, "mysql");
        map.put(3, "derby");

        String r1 = map.remove(1);
        boolean r2 = map.remove(2, "derby");
    }

    public static void replaceInMap() {
        Map<Integer, Melon> mapOfMelon = new HashMap<>();
        mapOfMelon.put(1, new Melon("Apollo", 3000));
        mapOfMelon.put(2, new Melon("Jade Dew", 3500));
        mapOfMelon.put(3, new Melon("Cantaloupe", 1500));
        //If success returns new element
        Melon melon = mapOfMelon.replace(2, new Melon("Gac", 1000));
        //boolean replace
        boolean melonB = mapOfMelon.replace(1, new Melon("Apollo", 3000), new Melon("Bitter", 4300));
        //replace all melons > 1000
        BiFunction<Integer, Melon, Melon> replaceMelon = (k, v) -> v.getWeight() > 1000 ? new Melon(v.getType(), 1000) : v;
        mapOfMelon.replaceAll(replaceMelon);

    }
}
