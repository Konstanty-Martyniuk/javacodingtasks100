package org.kot.chapter05;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MutableMelon {
    private final String type;
    private int weight;
}
