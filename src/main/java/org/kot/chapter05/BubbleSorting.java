package org.kot.chapter05;

import java.util.Arrays;
import java.util.Comparator;

public class BubbleSorting {
    public static void bubbleSort(int[] arr) {
        int arrLength = arr.length;

        for (int i = 0; i < arrLength - 1; i++) {
            for (int j = 0; j < arrLength - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }
            }
        }
        System.out.println(Arrays.toString(arr));
    }

    public static void bubbleSortWhile(int[] arr) {
        boolean isSorted = false;
        int temp;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < arr.length - 1; i++) {
                //DESC if (arr[i] < arr[i + 1])
                if (arr[i] > arr[i + 1]) {
                    temp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = temp;
                    isSorted = false;
                }
            }
        }
        System.out.println("Bubble sort");
        System.out.println(Arrays.toString(arr));
    }

    public static <T> void bubbleSortWithComparator(T arr[], Comparator<? super T> comparator) {
        boolean isSorted = false;
        T temp;

        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < arr.length; i++) {
                if (comparator.compare(arr[i], arr[i + 1]) > 0) {
                    temp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = temp;
                    isSorted = false;
                }
            }
        }
    }

}
