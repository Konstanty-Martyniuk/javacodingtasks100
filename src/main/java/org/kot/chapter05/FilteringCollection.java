package org.kot.chapter05;

import java.util.*;
import java.util.stream.Collectors;

public class FilteringCollection {
    public static void main(String[] args) {
        List<Melon> melons = new ArrayList<>() ;
        melons.add(new Melon("Apollo", 3000));
        melons.add(new Melon("Jade Dew", 3500));
        melons.add(new Melon("Cantaloupe", 1500));
        melons.add(new Melon("Gac", 1600));
        melons.add(new Melon("Hami", 1400));

        //lIST OF MELONS we want to get the data
        List<String> melonsByType = Arrays.asList("Apollo", "Gac", "Crenshaw", "Hami");

        //Stream solution
        List<Melon> result = melons.stream()
                .filter(t -> melonsByType.contains(t.getType()))
                .toList();

        //hashset faster method
        Set<String> melonsSetByType = new HashSet<>(melonsByType);
        List<Melon> resultWithHash = melons.stream()
                .filter(t -> melonsSetByType.contains(t.getType()))
                .toList();

    }
}
