package org.kot.chapter05;

import java.util.Arrays;

public class FillArray {
    static int[] arr = new int[10];

    public static void main(String[] args) {
        for(int i = 0; i < arr.length; i++) {
            arr[i] = 1;
        }

        Arrays.fill(arr, 1);

        //fill array, each element is set to the value of the previous element plus 1
        Arrays.setAll(arr, t -> {
            if (t == 0) {
                return arr[t];
            } else {
                return arr[t - 1] + 1;
            }
        });

        Arrays.setAll(arr, t -> {
            if (t % 2 == 0) {
                return arr[t] * arr[t];
            } else {
                return arr[t] - 1;
            }
        });

        //calculate sum with previous
        Arrays.parallelPrefix(arr, (t, q) -> t + q);
        Arrays.parallelPrefix(arr, Integer::sum);
    }

}
