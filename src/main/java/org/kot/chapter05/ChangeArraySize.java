package org.kot.chapter05;

import java.util.Arrays;

public class ChangeArraySize {
    //Add
    public static int[] add(int[] arr, int item) {
        int[] newArr = Arrays.copyOf(arr, arr.length + 1);
        newArr[newArr.length - 1] = item;
        return newArr;
    }

    //remove last
    public static int[] remove(int[] arr) {
        return Arrays.copyOf(arr, arr.length - 1);
    }

    //change size
    public static int[] resize (int [] arr, int length) {
        return Arrays.copyOf(arr, arr.length + length);
    }
}
