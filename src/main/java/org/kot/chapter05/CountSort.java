package org.kot.chapter05;

public class CountSort {
    public static void countSort(int[] arr, int maxValue) {
        int[] count =  new int[maxValue];

        for (int i = 0; i < arr.length; i++) {
            count[arr[i]] = count[arr[i]] + 1;
        }

        int arrayIndex = 0;
        for (int i = 0; i < count.length; i++) {
            for (int j = 0; j < count[i]; j++) {
                arr[arrayIndex] = i;
                arrayIndex++;
            }

        }
    }
}
