package org.kot.chapter05;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class ConvertCollectionToArray {
    public static void main(String[] args) {
        List<String> names = Arrays.asList("ana", "mario", "vio");
        Object[] namesArrayAsObjects = names.toArray(); //but this will give objects instead of String
        String[] namesArrayAsStringsBad = names.toArray(new String[names.size()]);
        String[] namesArrayAsStrings = names.toArray(new String[0]);
        //from JDK11
        String[] namesArrayAsStringsJDK11 = names.toArray(String[]::new);

        String[] namesArray = {"ana", "mario", "vio"};
        List<String> namesArrayAsList = List.of(namesArray);
        Set<String> nameesArrayAsSet = Set.of(namesArray);
    }
}
