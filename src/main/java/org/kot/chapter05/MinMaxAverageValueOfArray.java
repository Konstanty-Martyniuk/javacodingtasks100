package org.kot.chapter05;

import java.util.Arrays;
import java.util.Comparator;

public class MinMaxAverageValueOfArray {
    int[] integers = {1, 2, 3, 4};
    Melon[] melons = {new Melon ("Horned", 1500),
            new Melon ("Gac", 2200),
            new Melon("Gac", 2100)};

    public static int getMax(int[] arr) {
        int max = arr[0];
        for (var number : arr) {
            max = Math.max(max, number);
        }
        return max;
    }

    //via Stream
    int max = Arrays.stream(integers).max().getAsInt();

    public static <T> T max(T[] arr, Comparator<? super T> comparator) {
        T max = arr[0];
        for (var element : arr) {
            if (comparator.compare(element, max) > 0) {
                max = element;
            }
        }
        return max;
    }

    //via stream with comparator
    Comparator<Melon> byWeight = Comparator.comparing(Melon::getWeight);
    Melon theBiggest = Arrays.stream(melons).max(byWeight).orElseThrow();

    //average
    double average = Arrays.stream(integers).average().getAsDouble();
}
