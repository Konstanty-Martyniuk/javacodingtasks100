package org.kot.chapter05;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PartitioningArrayBy {
     /*
   REMOVE METHODS
    */

    public static void main(String[] args) {
        List<Melon> melons = new ArrayList<>() ;
        melons.add(new Melon ("Apollo", 3000));
        melons.add(new Melon("Jade Dew", 3500));
        melons.add(new Melon("Cantaloupe", 1500));
        melons.add(new Melon("Gac", 1600));
        melons.add(new Melon("Hami", 1400));

        melons.removeIf(t -> t.getWeight() < 3000);

        //via Stream
        List<Melon> filteredMelons = melons.stream()
                .filter(t -> t.getWeight() >= 3000)
                .toList();

        //Partitioning instead of deleting
        Map<Boolean, List<Melon>> separatedMelons = melons.stream()
                .collect(Collectors.partitioningBy(t -> t.getWeight() > 3000));
        List<Melon> weightLessThan3000 = separatedMelons.get(false);
        List<Melon> weightGreaterThan3000 = separatedMelons.get(true);
    }
}
