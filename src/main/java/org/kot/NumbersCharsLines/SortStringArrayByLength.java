package org.kot.NumbersCharsLines;

import java.util.Arrays;
import java.util.Comparator;

public class SortStringArrayByLength {
    enum Sort {
        ASC, DESC
    }
    public static void sortStringArrayByLength(String[] strings, Sort direction) {
        if(direction.equals(Sort.ASC)) {
            Arrays.sort(strings, (s1,s2) -> s1.length() - s2.length());
        } else {
            Arrays.sort(strings, (s1,s2) -> s2.length() - s1.length());
        }
    }

    public static void sortStringArrayByLengthWithComparator(String[] strings, Sort direction) {
        if(direction.equals(Sort.ASC)) {
            Arrays.sort(strings, Comparator.comparing(String::length));
        } else {
            Arrays.sort(strings, Comparator.comparing(String::length).reversed());
        }
    }

    public static String[] sortStringArrayByLengthInNewArray(String[] strings, Sort direction) {
        if (direction.equals(Sort.ASC)) {
            return Arrays.stream(strings)
                    .sorted(Comparator.comparing(String::length))
                    .toArray(String[]::new);
        } else {
            return Arrays.stream(strings)
                    .sorted(Comparator.comparing(String::length).reversed())
                    .toArray(String[]::new);
        }
    }
}
