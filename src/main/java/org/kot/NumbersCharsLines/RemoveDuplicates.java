package org.kot.NumbersCharsLines;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class RemoveDuplicates {
    public static String removeDuplicates(String line) {
        char[] chars = line.toCharArray();
        StringBuilder sb = new StringBuilder();

        for(char ch : chars) {
            if(sb.indexOf(String.valueOf(ch)) == -1) {
                sb.append(ch);
            }
        }
        return sb.toString();
    }

    public static String removeDuplicatesWithHashSet(String line) {
        char[] chars = line.toCharArray();
        StringBuilder sb = new StringBuilder();
        Set<Character> charSet = new HashSet<>();

        for (char c : chars) {
            if(charSet.add(c))  {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    public static String removeDuplicatesWithStream(String line) {
        return line.chars()
                .distinct()
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    public static String removeDuplicatesStream8(String line) {
        return Arrays.stream(line.split(""))
                .distinct ()
                .collect(Collectors.joining());
    }
}
