package org.kot.NumbersCharsLines;

public class CheckIfPalindrome {
    public static boolean checkIfPalindromeWithStream(String line) {
        return line.contentEquals(new StringBuilder(line).reverse());
    }

    public static boolean isPalindrome(String line) {
        int length = line.length();
        for(int i = 0; i < length / 2; i++) {
            if(line.charAt(i) != line.charAt(length - i - 1)) {
                return false;
            }
        }
        return true;
    }
}
