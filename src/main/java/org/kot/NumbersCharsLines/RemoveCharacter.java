package org.kot.NumbersCharsLines;

import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class RemoveCharacter {
    //Use pattern.quote to escape special characters
    public static String removeCharacterWithPattern(String line, char ch) {
        return line.replaceAll(Pattern.quote(String.valueOf(ch)), "");
    }

    public static String removeCharacterWithStringBuilder(String line, char ch) {
        StringBuilder sb = new StringBuilder();
        for(char c : line.toCharArray()) {
            if(c != ch) {
                sb.append(c);
            }
        }
        return sb.toString();
    }

    public static String removeCharacterWithStream(String line, char ch) {
        return line.chars()
                .filter(c -> c != ch)
                .mapToObj(c -> String.valueOf((char)c))
                .collect(Collectors.joining());
    }
}
