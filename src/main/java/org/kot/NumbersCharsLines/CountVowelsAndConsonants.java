package org.kot.NumbersCharsLines;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.AbstractMap.SimpleEntry;
public class CountVowelsAndConsonants {
    private static final Set<Character> ALLVOWELLS = new HashSet<>(Arrays.asList('a', 'e', 'i', 'o', 'u'));

    public static SimpleEntry<Integer, Integer> countVowelsAndConsonants(String line) {
        line = line.toLowerCase();
        int vowels = 0;
        int consonants = 0;
        for(Character ch : line.toCharArray()) {
            if(ALLVOWELLS.contains(ch)) {
                vowels++;
            } else if(Character.isLetter(ch)) {
                consonants++;
            }
        }
        return new SimpleEntry<>(vowels, consonants);
    }

    public static SimpleEntry<Integer, Integer> countVowelsAndConsonantsWithStream(String line) {
        line = line.toLowerCase();
        int vowels = line.chars()
                .filter(ch -> ALLVOWELLS.contains((char)ch))
                .sum();
        int consonants = line.chars()
                .filter(Character::isLetter)
                .sum() - vowels;

        return new SimpleEntry<>(vowels, consonants);
    }
}
