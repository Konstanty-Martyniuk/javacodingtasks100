package org.kot.NumbersCharsLines;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class CountDuplicateChars {
    public Map<Character, Long> countDuplicateCharacters(String line) {
        Map<Character, Long> result = new HashMap<>();
        for (int i = 0; i < line.length(); i++) {
            char ch = line.charAt(i);
            result.compute(ch, (k, v) -> (v == null) ? 1 : ++v);
        }
        return result;
    }

    public Map<Character, Long> countDuplicateCharsWithStream(String line) {
        return line.chars()
                .mapToObj(c -> (char) c)
                .collect(Collectors.groupingBy(c -> c, Collectors.counting()));
    }

    // Working with ASCII and Unicode
    public Map<String, Long> countDuplicateCharsWithCodePoint(String line) {
        Map<String, Long> result = new HashMap<>();
        for (int i = 0; i < line.length(); i++) {
            int codePoint = line.codePointAt(i);
            String str = String.valueOf(Character.toChars(codePoint));
            if (Character.charCount(codePoint) == 2) {
                i++;
            }
            result.compute(str, (k, v) -> (v == null) ? 1 : ++v);
        }
        return result;
    }

    public Map<String, Long> countDuplicateCharsWithCodePointWithStream(String line) {
        return line.codePoints()
                .mapToObj(c -> String.valueOf(Character.toChars(c)))
                .collect(Collectors.groupingBy(c -> c, Collectors.counting()));
    }
}
