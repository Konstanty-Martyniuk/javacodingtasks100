package org.kot.NumbersCharsLines;

public class CheckIfOnlyDigits {
    public static boolean checkIfOnlyDigitsWithRegex(String line) {
        return line.matches("\\d+");
    }

    public static boolean checkIfOnlyDigitsWithStream(String line) {
        return line.chars().allMatch(Character::isDigit);
    }
}
