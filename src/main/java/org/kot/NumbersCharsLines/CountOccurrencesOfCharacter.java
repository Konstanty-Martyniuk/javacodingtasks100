package org.kot.NumbersCharsLines;

public class CountOccurrencesOfCharacter {
    public static long countOccurrencesOfCharacterWithStream(String line, char ch) {
        return line.chars()
                .filter(c -> c == ch)
                .count();
    }

}
