package org.kot.NumbersCharsLines;

import java.util.AbstractMap.SimpleEntry;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

public class FindMaxOccurrence {
     public static SimpleEntry<Character, Integer> findMaxOccurrence(String line) {
         Map<Character, Integer> countMap = new HashMap<>();
         char[] chars = line.toCharArray();
         for (char aChar : chars) {
             if (Character.isLetter(aChar)) {
                 countMap.put(aChar, countMap.getOrDefault(aChar, 0) + 1);
             }
         }
         int maxOccurrence = Collections.max(countMap.values());
         for(Map.Entry<Character, Integer> entry : countMap.entrySet()) {
             if(entry.getValue() == maxOccurrence) {
                 return new SimpleEntry<>(entry.getKey(), entry.getValue());
             }
         }
         return new SimpleEntry<>(' ', -1);
     }

     public static SimpleEntry<Character, Integer> findMaxOccurrenceWithASCIICodes(String line) {
         int[] countArray = new int[256];
         int maxOccurrence = -1;
         char maxChar = ' ';
         char[] chars = line.toCharArray();

         for(char currentChar : chars) {
             if(Character.isLetter(currentChar)) {
                 countArray[currentChar]++;
                 if(countArray[currentChar] > maxOccurrence) {
                     maxOccurrence = countArray[currentChar];
                     maxChar = currentChar;
                 }
             }
         }
         return new SimpleEntry<>(maxChar, maxOccurrence);
     }

    public static SimpleEntry<Character, Long> findMaxOccurrenceWithStream(String line) {
         return line.chars()
                 .filter(Character::isLetter)
                 .mapToObj(c -> (char)c)
                 .collect(groupingBy(c -> c, counting()))
                 .entrySet()
                 .stream()
                 .max(Map.Entry.comparingByValue())
                 .map(e -> new SimpleEntry<>(e.getKey(), e.getValue()))
                 .orElse(new SimpleEntry<>(' ', -1L));
    }
}
