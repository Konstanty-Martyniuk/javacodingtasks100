package org.kot.NumbersCharsLines;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CountsAndContains {
    public static boolean stringContainsText(String line, String text) {
        return line.contains(text);
    }

    // Just for the sake of the example, this method is not used in the application
    public static boolean stringContains(String line, String text) {
        return line.indexOf(text) != -1;
    }

    public static int countOccurrencesOfStringInString(String line, String textToFind) {
        int count = 0;
        int length = textToFind.length();
        for(int i = 0; i < line.length() - length; i++) {
            if(line.substring(i, i + length).equals(textToFind)) {
                count++;
            }
        }
        return count;
    }

    public static int betterCountOccurrencesOfStringInStringWithPattern(String line, String textToFind) {
        Pattern pattern = Pattern.compile(textToFind);
        Matcher matcher = pattern.matcher(line);

        int count = 0;
        int position = 0;
        while(matcher.find(position)) {
            count++;
            position = matcher.start() + 1;
        }
        return count;
    }

    public static boolean isAnagram(String line1, String line2) {
        if(line1.length() != line2.length()) {
            return false;
        }

        int[] allChars = new int[256];
        char[] line1Chars = line1.replaceAll("\\s", "").toLowerCase(Locale.ROOT).toCharArray();
        char[] line2Chars = line2.replaceAll("\\s", "").toLowerCase(Locale.ROOT).toCharArray();

        for(int i = 0; i < line1Chars.length; i++) {
            allChars[line1Chars[i]]++;
            allChars[line2Chars[i]]--;
        }

        for (int currentChar : allChars) {
            if(currentChar != 0) return false;
        }
        return true;
    }

    public static String longestCommonPrefix(String[] lines) {
        if(lines.length == 0) return "";
        if (lines.length == 1) return lines[0];

        int fistLineLength = lines[0].length();
        for (int prefixLength = 0; prefixLength < fistLineLength; prefixLength++) {
            char currentChar = lines[0].charAt(prefixLength);
            for (int i = 1; i < lines.length; i++) {
                if(prefixLength >= lines[i].length() || lines[i].charAt(prefixLength) != currentChar) {
                    return lines[0].substring(0, prefixLength);
                }
            }
        }
        return lines[0];
    }
}
