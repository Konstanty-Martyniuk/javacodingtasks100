package org.kot.NumbersCharsLines;

public class ReversCharsAndWords {

    public String reverseCharsInWords(String line) {
        String[] words = line.split(" ");
        StringBuilder reversedString = new StringBuilder();

        for(String word : words) {
            StringBuilder reversedWord = new StringBuilder(word);
            reversedString.append(reversedWord.reverse()).append(" ");
        }
        return reversedString.toString();
    }

    public String reverseCharsAndWordsWithStream(String line) {
        return new StringBuilder(line).reverse().toString();
    }
}
