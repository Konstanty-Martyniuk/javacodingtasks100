package org.kot.NumbersCharsLines;

import java.util.HashSet;
import java.util.Set;

public class PermuteAndPrint {
    public static Set<String> permuteAndStore(String str) {
        return permute("", str);
    }

    private static Set<String> permute(String prefix, String str) {
        Set<String> permutations = new HashSet<>();
        int length = str.length();
        if(length == 0) {
            permutations.add(prefix);
        } else {
            for(int i = 0; i < length; i++) {
               permutations.addAll(permute(prefix + str.charAt(i), str.substring(0, i)
                       + str.substring(i + 1, length)));
            }
        }
        return permutations;
    }
}
