package org.kot.chapter07;

import lombok.AllArgsConstructor;
import lombok.Data;


@AllArgsConstructor
public class MelonMutable{
    private String type;
    private int weight;
    private boolean ripe;

    public String getType() {
        return type;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getWeight() {
        return weight;
    }
}
