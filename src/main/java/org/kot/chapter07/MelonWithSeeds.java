package org.kot.chapter07;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.util.Collections;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
public class MelonWithSeeds {

    private String type;
    private int weight;

    public List<MelonWithSeeds> cultivate(String type, Seed seed, int noOfSeeds) {
        System.out.println("The cultivate() method was invoked ...");
        return Collections.nCopies(noOfSeeds, new MelonWithSeeds("Gac", 5));
    }

    @Override
    public String toString() {
        return type + "(" + weight + "g)";
    }
}
