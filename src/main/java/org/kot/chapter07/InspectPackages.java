package org.kot.chapter07;

import java.io.File;
import java.util.Arrays;
import java.util.List;

public class InspectPackages {
    public static void main(String[] args) throws ClassNotFoundException {
        Class<?> clazz = Class.forName("java.lang.Integer");
        Package packageOfClazz = clazz.getPackage();

        String packageNameOfClazz = packageOfClazz.getName();
        System.out.println(packageNameOfClazz); //java.lang

        File file = new File(".");
        Package packageOfFile = file.getClass().getPackage();
        String packageNameOfFile = packageOfFile.getName(); //java.io

        List<String> packagesSamePrefix = fetchPackagesByPrefix("java.util");
        System.out.println(packagesSamePrefix.toString());
    }

    //get all packages
    public static List<String> fetchPackagesByPrefix(String prefix) {
        return Arrays.stream(Package.getPackages())
                .map(Package::getName)
                .filter(p -> p.startsWith(prefix))
                .toList();
    }

}
