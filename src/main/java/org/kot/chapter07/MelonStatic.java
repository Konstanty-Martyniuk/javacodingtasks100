package org.kot.chapter07;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class MelonStatic {
    private final String type;
    @Getter
    @Setter
    private int weight;

    public void eat() {
    }

    public void weighsIn() {
    }

    public static void cultivate(Seed seeds) {
        System.out.println("The cultivate() method was invoked ...");
    }

    public static void peel(Slice slice) {
        System.out.println("The peel() method was invoked ...");
    }

    @Override
    public String toString() {
        return type + "(" + weight + "g)";
    }
}
