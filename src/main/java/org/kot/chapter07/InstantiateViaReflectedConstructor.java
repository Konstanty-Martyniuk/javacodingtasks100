package org.kot.chapter07;

import java.awt.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class InstantiateViaReflectedConstructor {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, InstantiationException, IllegalAccessException {
        Class<Car> clazz = Car.class;

        Constructor<Car> emptyConstructor = clazz.getConstructor();
        Constructor<Car> idNameConstructor = clazz.getConstructor(int.class, String.class);
        Constructor<Car> idColorConstructor = clazz.getConstructor(int.class, Color.class);
        Constructor<Car> idNameColorConstructor = clazz.getConstructor(int.class, String.class, Color.class);

        Car carViaEmptyConstructor = emptyConstructor.newInstance();
        Car carViaIdNameConstructor = idNameConstructor.newInstance(1, "Volvo");
        Car carViaIdColorConstructor = idColorConstructor.newInstance(2, new Color(0,0,0));
        Car carViaIdNameColorConstructor = idNameColorConstructor.newInstance(3, "Mazda", new Color(0,0,0));


        Class<Cars> carsClass = Cars.class;
        Constructor<Cars> emptyCarsConstructor = carsClass.getDeclaredConstructor();
        //as constructor is private, we have to fix it
        emptyCarsConstructor.setAccessible(true);
        Cars viaEmptyCarsConstructor = emptyCarsConstructor.newInstance();
        //to disable this option, Cars private constructor should throw error throw new AssertionError("cannot create");
    }
}
