package org.kot.chapter07;

import org.kot.chapter05.Melon;

import java.lang.invoke.MethodHandle;
import java.lang.reflect.*;
import java.sql.CallableStatement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException, NoSuchMethodException, InvocationTargetException, InstantiationException {
        Pair pair = new Pair(1, 1);

        //get class name
        Class<?> clazz = pair.getClass();
        System.out.println("class name: " + clazz.getName()); //org.kot.chapter07.Pair
        System.out.println("simple name: " + clazz.getSimpleName()); //Pair
        System.out.println("canonical name: " + clazz.getCanonicalName()); //org.kot.chapter07.Pair


        //get modifiers
        int modifiers = clazz.getModifiers();

        System.out.println("Is public? " + Modifier.isPublic(modifiers)); // true
        System.out.println("Is final? " + Modifier.isFinal(modifiers)); // true
        System.out.println("Is abstract? " + Modifier.isAbstract(modifiers)); // false


        //get interfaces
        Class<?>[] interfaces = clazz.getInterfaces();
        System.out.println("Interfaces: " + Arrays.toString(interfaces)); //[interface java.lang.Comparable]
        //simple name
        System.out.println("Simple Interface name: " + interfaces[0].getSimpleName()); //Comparable


        //get constructors
        Constructor<?>[] constructors = clazz.getConstructors();
        System.out.println("Pair clazz constructors: " + Arrays.toString(constructors)); //[public org.kot.chapter07.Pair(java.lang.Object,java.lang.Object)]
        //get all constructors
        Constructor<?>[] constructorsAll = clazz.getDeclaredConstructors();
        System.out.println("Pair clazz constructors: " + Arrays.toString(constructors));


        //get fields
        Field[] fields = clazz.getDeclaredFields();
        System.out.println("Pair fields: " + Arrays.toString(fields)); //[public org.kot.chapter07.Pair(java.lang.Object,java.lang.Object)]
        //get just names
        List<String> justFieldNames = getFieldNames(fields);
        System.out.println("Just field names: " + justFieldNames);


        //get methods
        Method[] methods = clazz.getMethods();
        List<String> methodNames = getMethodNames(methods);
        System.out.println("Methods: " + methodNames);


        //get module
        Module module = clazz.getModule();
        System.out.println("module: " + module.getName());


        //get super class
        Class<?> superClass = clazz.getSuperclass();
        System.out.println("SuperClass: " + superClass.getSimpleName());


        //Invoke instance method
        Method cultivateMethod = MelonWithSeeds.class.getDeclaredMethod(
                "cultivate", String.class, Seed.class, int.class);
        MelonWithSeeds instanceMelonWithSeeds = MelonWithSeeds.class.getDeclaredConstructor().newInstance();
        List<MelonWithSeeds> cultivateMelons = (List<MelonWithSeeds>) cultivateMethod.invoke(
                instanceMelonWithSeeds, "Gac", new Seed(), 10);
        System.out.println("Invoke instance method" + cultivateMelons);


        //get static methods
        List<Method> staticMethods = new ArrayList<>();
        Class<MelonStatic> clazzStatic = MelonStatic.class;
        Method[] methodsAll = clazzStatic.getDeclaredMethods();

        for (var method : methodsAll) {
            if (Modifier.isStatic(method.getModifiers())) {
                staticMethods.add(method);
            }
        }
        System.out.println(staticMethods);

        Method methodPeel = clazzStatic.getDeclaredMethod("peel", Slice.class);
        methodPeel.invoke(null, new Slice());
    }

    //helper for fields name
    public static List<String> getFieldNames(Field[] fields) {
        return Arrays.stream(fields)
                .map(Field::getName)
                .toList();
    }

    //helper for methods name
    public static List<String> getMethodNames(Method[] methods) {
        return Arrays.stream(methods)
                .map(Method::getName)
                .toList();
    }
}
