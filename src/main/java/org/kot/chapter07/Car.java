package org.kot.chapter07;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.awt.Color;

@NoArgsConstructor
@Data
public class Car {
    private int id;
    private String name;
    private Color color;

    public Car(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Car(int id, Color color) {
        this.id = id;
        this.color = color;
    }

    public Car(int id, String name, Color color) {
        this.id = id;
        this.name = name;
        this.color = color;
    }

}
