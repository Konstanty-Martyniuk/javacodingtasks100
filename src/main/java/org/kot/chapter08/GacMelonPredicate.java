package org.kot.chapter08;

public class GacMelonPredicate implements MelonPredicate {
    @Override
    public boolean test(MelonFunc melon) {
        return "gac".equalsIgnoreCase(melon.getType());
    }
}
