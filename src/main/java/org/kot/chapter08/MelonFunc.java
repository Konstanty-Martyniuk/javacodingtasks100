package org.kot.chapter08;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MelonFunc {
    private final String type;
    private final int weight;
    private final String origin;
}
