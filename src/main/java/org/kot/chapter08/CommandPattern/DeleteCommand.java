package org.kot.chapter08.CommandPattern;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class DeleteCommand implements Command {
    private final IODevice action;
    @Override
    public void execute() {
        action.delete();
    }
}
