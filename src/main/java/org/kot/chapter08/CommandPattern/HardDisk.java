package org.kot.chapter08.CommandPattern;

public class HardDisk implements IODevice {
    @Override
    public void copy() {
        System.out.println("Copying files...");
    }

    @Override
    public void delete() {
        System.out.println("Deleting files...");
    }

    @Override
    public void move() {
        System.out.println("Moving files...");
    }
}
