package org.kot.chapter08.CommandPattern;

public class Main {
    public static void main(String[] args) {
        HardDisk hardDisk = new HardDisk();
        Sequence sequence = new Sequence();
        sequence.recordSequence(new CopyCommand(hardDisk));
        sequence.recordSequence(new DeleteCommand(hardDisk));
        sequence.recordSequence(hardDisk::move);
        sequence.runSequence();

    }
}
