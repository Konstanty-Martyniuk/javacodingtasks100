package org.kot.chapter08.CommandPattern;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class MoveCommand implements Command {
    private final IODevice action;
    @Override
    public void execute() {
        action.move();
    }
}
