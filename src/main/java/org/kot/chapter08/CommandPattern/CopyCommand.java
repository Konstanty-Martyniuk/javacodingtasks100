package org.kot.chapter08.CommandPattern;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class CopyCommand implements Command {
    private final IODevice action;
    @Override
    public void execute() {
        action.copy();
    }
}
