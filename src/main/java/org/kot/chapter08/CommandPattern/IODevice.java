package org.kot.chapter08.CommandPattern;

public interface IODevice {
    void copy();
    void delete();
    void  move();
}
