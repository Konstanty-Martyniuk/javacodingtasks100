package org.kot.chapter08.CommandPattern;

public interface Command {
    void execute();
}
