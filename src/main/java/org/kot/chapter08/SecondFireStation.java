package org.kot.chapter08;

public class SecondFireStation implements FireObserver {
    @Override
    public void fire(String address) {
        if (address.contains("second")) {
            System.out.println("Second FireStation is ready to save");
        }
    }
}

