package org.kot.chapter08;

public class WhitespaceRemover implements RemoveStrategy {
    @Override
    public String execute(String s) {
        return s.replaceAll("\\s", "");
    }
}
