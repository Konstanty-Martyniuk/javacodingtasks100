package org.kot.chapter08;

import java.util.Map;
import java.util.function.Supplier;

public final class MelonFactory {
    private MelonFactory() {
        throw new AssertionError("Cannot be instantiated");
    }

    private static final TriFunction<String, Integer, String, MelonForFactory> MELON_FOR_FACTORY = MelonForFactory::new;
    private static final Map<String, Supplier<Fruit>> MELONS = Map.of(
            "Gac", Gac::new, "Hemi", Hemi::new, "Cantaloupe", Cantaloupe::new);


    public static Fruit newInstance(Class<?> clazz) throws IllegalAccessException {
        Supplier<Fruit> supplier = MELONS.get(clazz.getSimpleName());

        if (supplier == null) {
            throw new IllegalAccessException("Incorrect clazz: " + clazz);
        }
        return supplier.get();
    }

    public static Fruit newInstance(String name, Integer weight, String color) {
        return MELON_FOR_FACTORY.apply(name, weight, color);
    }

}
