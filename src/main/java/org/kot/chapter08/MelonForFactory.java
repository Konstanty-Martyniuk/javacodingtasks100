package org.kot.chapter08;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class MelonForFactory implements Fruit {
    private final String type;
    private final int weight;
    private final String color;
}
