package org.kot.chapter08;

public final class Remover {
    private Remover() {
        throw new AssertionError("Cannot create object");
    }

    public static String remove(String s, RemoveStrategy strategy) {
        return strategy.execute(s);
    }
}
