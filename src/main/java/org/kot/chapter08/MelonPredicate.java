package org.kot.chapter08;

public interface MelonPredicate {
    boolean test(MelonFunc melon);
}
