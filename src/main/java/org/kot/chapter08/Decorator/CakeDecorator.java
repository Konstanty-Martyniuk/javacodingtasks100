package org.kot.chapter08.Decorator;

import lombok.AllArgsConstructor;

import java.util.function.Function;
import java.util.stream.Stream;

@AllArgsConstructor
public class CakeDecorator {
    private Function<Cake, Cake> decorator;

    public CakeDecorator(Function<Cake, Cake>... decorations) {
        reduceDecoration(decorations);
    }

    public Cake decorate(Cake cake) {
        return decorator.apply(cake);
    }

    private void reduceDecoration(Function<Cake, Cake>... decorations) {
        decorator = Stream.of(decorations)
                .reduce(Function.identity(), Function::andThen);
    }
}
