package org.kot.chapter08.Decorator;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class Cake {
    @Getter
    private final String decorations;

    public Cake decorate(String decoration) {
        return new Cake(getDecorations() + decoration);
    }


}
