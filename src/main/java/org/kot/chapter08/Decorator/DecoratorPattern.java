package org.kot.chapter08.Decorator;

public class DecoratorPattern {
    public static void main(String[] args) {
        CakeDecorator nutsAndCream = new CakeDecorator(
                c -> c.decorate(" with nuts"),
                c -> c.decorate(" and cream")
        );
        Cake cake = nutsAndCream.decorate(new Cake("Basic cake"));
        System.out.println(cake.getDecorations());
    }
}
