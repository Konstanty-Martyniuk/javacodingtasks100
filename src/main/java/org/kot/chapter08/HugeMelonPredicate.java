package org.kot.chapter08;

public class HugeMelonPredicate implements MelonPredicate {
    @Override
    public boolean test(MelonFunc melon) {
        return melon.getWeight() > 5000;
    }
}
