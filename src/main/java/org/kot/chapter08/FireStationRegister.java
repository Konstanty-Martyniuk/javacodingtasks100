package org.kot.chapter08;

public interface FireStationRegister {
    void registerFireStation(FireObserver fo);
    void notifyFireStation(String address);
}
