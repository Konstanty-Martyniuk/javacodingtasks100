package org.kot.chapter08;

public interface FireObserver {
    void fire(String address);
}
