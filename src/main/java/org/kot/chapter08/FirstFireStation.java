package org.kot.chapter08;

public class FirstFireStation implements FireObserver {
    @Override
    public void fire(String address) {
        if (address.contains("first")) {
            System.out.println("First FireStation is ready to save");
        }
    }
}
