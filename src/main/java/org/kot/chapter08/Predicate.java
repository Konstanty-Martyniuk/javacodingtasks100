package org.kot.chapter08;
@FunctionalInterface
public interface Predicate<T> {
    boolean test(T t);
}
