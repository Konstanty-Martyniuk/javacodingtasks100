package org.kot.chapter08;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        List<MelonFunc> melons = Arrays.asList(
                new MelonFunc("Gac", 5500, "Europe"),
                new MelonFunc("Bailan", 6000, "China"),
                new MelonFunc("Watermelon", 1200, "Europe"),
                new MelonFunc("Gac", 3400, "US"),
                new MelonFunc("Bailan", 1300, "China"));
        List<MelonFunc> gaces = Filters.filterMelonsFunc(melons, new GacMelonPredicate());
        List<MelonFunc> huge = Filters.filterMelonsFunc(melons, new HugeMelonPredicate());
        //better but still not ok
        List<MelonFunc> europeans = Filters.filterMelonsFunc(melons,
                new MelonPredicate() {
                    @Override
                    public boolean test(MelonFunc melon) {
                        return "europe".equalsIgnoreCase(melon.getOrigin());
                    }
                });
        //lambda version
        List<MelonFunc> europeansLambda = Filters.filterMelonsFunc(melons,
                m -> "europe".equalsIgnoreCase(m.getOrigin()));

        List<MelonFunc> watermelons = Filters.filter(melons,
                m -> "watermelons".equalsIgnoreCase(m.getType()));

        List<Integer> numbers = List.of(1,2,3,4,5,6,20, 17, 67);
        List<Integer> smallerThan10 = Filters.filter(numbers, n -> n < 10);

        MelonForFactory melonForFactory = (MelonForFactory) MelonFactory.newInstance("Gac", 2000, "red");


        //Remove strategy
        String text = "This is text from 25 May 2024";
        String textWithoutNumbers = Remover.remove(text, new NumberRemover());
        //Better solution to use lambda
        String textWithoutNumbersLambda = Remover.remove(text, s -> s.replaceAll("\\d", ""));


        //Pizza
        Pizza nPizza = new Pizza();
        PizzaMaker nPizzaMaker = new NeapolitanPizza();
        nPizzaMaker.make(nPizza);

        Pizza pizzaLambda = new Pizza();
        new PizzaLambda().make(pizzaLambda, p -> System.out.println("Adding your best toppings"));


        //Fire Station
        FireStation fireStation = new FireStation();
        fireStation.registerFireStation(new FirstFireStation());
        fireStation.registerFireStation(new SecondFireStation());

        fireStation.notifyFireStation("first has fire!");
        //better with lambda
        fireStation.registerFireStation(address -> {
            if (address.contains("first")) {
                System.out.println("First Fire Station will take care!");
            }
        });


        //LoanPattern
        double xPlusYMinusZ = Formula.compute(sc -> sc.add().add().minus().result());
    }


}
