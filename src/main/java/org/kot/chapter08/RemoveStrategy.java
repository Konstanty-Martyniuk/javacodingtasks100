package org.kot.chapter08;

@FunctionalInterface
public interface RemoveStrategy {
    String execute(String s);
}
