package org.kot.chapter08;

public abstract class PizzaMaker {
    public void make(Pizza pizza) {
        makeDough(pizza);
        addTopIngredients(pizza);
        bake(pizza);
    }

    private void makeDough(Pizza pizza) {
        System.out.println("Preparing dough");
    }

    private void bake(Pizza pizza) {
        System.out.println("Baking pizza");
    }

    public abstract void addTopIngredients(Pizza pizza);
}
