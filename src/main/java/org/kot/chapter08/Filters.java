package org.kot.chapter08;

import java.util.ArrayList;
import java.util.List;

public class Filters {
    public static List<MelonFunc> filterMelonsFunc (List<MelonFunc> melons, MelonPredicate predicate) {
        List<MelonFunc> result = new ArrayList<>();
        for (var melon : melons) {
            if (melon != null && predicate.test(melon)) {
                result.add(melon);
            }
        }
        return result;
    }

    public static <T> List<T> filter(List<T> list, Predicate<T> predicate) {
        List<T> result = new ArrayList<>();
        for (T t : list) {
            if (t != null && predicate.test(t)) {
                result.add(t);
            }
        }
        return result;
    }
}
