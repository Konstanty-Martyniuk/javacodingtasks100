package org.kot.chapter08.CascadedBuilderPattern;

public class Main {
    public static void main(String[] args) {
        Delivery.deliver(d -> d.firstname("Konstanty")
                .lastname("Martyniuk")
                .address("My address")
                .content("new PC")
        );
    }
}
