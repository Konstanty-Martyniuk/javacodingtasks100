package org.kot.chapter09.InfinitiveStreams;

import java.security.SecureRandom;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class InfinitiveStream {
    public static void main(String[] args) {
        Stream<Integer> infStream = Stream.iterate(1, i -> i + 1);
        List<Integer> result = infStream
                .filter(i -> i % 2 == 0)
                .limit(10)
                .toList();

        //advanced inf stream
        Stream<Integer> infStreamWithPredicate = Stream.iterate(1, i -> i <= 10, i -> i + 1);
        List<Integer> result2 = infStreamWithPredicate
                .filter(i -> i % 2 == 0)
                .toList();

        //combine iterate + limit
        Stream<Integer> infStreamItLi = Stream.iterate(1, i -> i <= 10, i -> i + i % 2 == 0
                ? new Random().nextInt(20) : -1 * new Random().nextInt(10));
        List<Integer> result3 = infStreamItLi
                .limit(25)
                .toList();


        //unlimited stream of pseudo random numbers
        IntStream randomInfStream = new Random().ints(1, 100);
        List<Integer> result4 = randomInfStream
                .filter(i -> i % 2 == 0)
                .limit(10)
                .boxed().toList();

        //generate random string
        IntStream randomInfStream2 = new Random().ints(20, 48, 126);
        String resultRandomChars = randomInfStream2
                .mapToObj(n -> String.valueOf((char) n))
                .collect(Collectors.joining());
        System.out.println(resultRandomChars);

        Supplier<String> passwordSupplier = InfinitiveStream::randomPassword;
        Stream<String> passwordStream = Stream.generate(passwordSupplier);
        List<String> passwordList = passwordStream
                .limit(10)
                .toList();
        System.out.println(passwordList);


        //takeWhile (Predicated super Т> predicate)
        List<Integer> resultTakeWhile = IntStream
                .iterate(1, i -> i + 1)
                .takeWhile(i -> i <= 10)
                .boxed()
                .toList();

        List<Integer> result5 = new Random().ints(1, 100)
                .filter(i -> i % 2 == 0)
                .takeWhile(i -> i >= 50)
                .boxed()
                .toList();


        //Stream.dropWhile (Predicated super T>
        List<Integer> result6 = IntStream
                .iterate(1, i -> i + 1)
                .dropWhile(i -> i <= 10)
                .limit(5)
                .boxed()
                .toList();
        System.out.println(result6);

        //surprise bitch! After first "false" dropWhile stop working
        List<Integer> resultOMG = new Random().ints (1, 100)
                .filter(i -> i % 2 == 0)
                .dropWhile(i -> i < 50)
                .limit(5)
                . boxed ()
                .toList();
        System.out.println(resultOMG); //[86, 36, 48, 10, 60]
    }
    //stream.generate (Supplier<? extends T> s)
    private static String randomPassword() {
        String chars = "abcd0123!%$^";

        return new SecureRandom().ints(8, 0, chars.length())
                .mapToObj(i -> String.valueOf(chars.charAt(i)))
                .collect(Collectors.joining());
    }

}
