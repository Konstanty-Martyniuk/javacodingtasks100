package org.kot.chapter09.TestHighOrderFunctionsAndLambdas;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.assertj.core.api.Assertions.assertThat;
import static org.kot.chapter09.TestHighOrderFunctionsAndLambdas.TestHighOrderFunctions.*;

class TestHighOrderFunctionsTest {

    @Test
    void testReplacer() throws Exception {
        List<String> names = Arrays.asList("Ann a 15", "Mir el 28", "D oru 33");

        List<String> resultWs = replace(names, s -> s.replaceAll("\\s", ""));
        List<String> resultNr = replace(names, s -> s.replaceAll("\\d", ""));

        assertEquals(Arrays.asList("Anna15", "Mirel28", "Doru33"), resultWs);
        assertEquals(Arrays.asList("Ann a ", "Mir el ", "D oru "), resultNr);
    }

    @Test
    void testReduceStrings() throws Exception {
        Function<String, String> f1 = String::toUpperCase;
        Function<String, String> f2 = s -> s.concat(" DONE");

        Function<String, String> f = reduceStrings(f1, f2);

        assertEquals("TEST DONE", f.apply("test"));
    }

    @Test
    public void testFirstAndLastChar() throws Exception {
        String text = "Lambda";
        String result = firstAndLastChar.apply(text);

        assertEquals("La", result);
    }

    @Test
    public void testRndStringFromString() throws Exception {
        String str1 = "Some";
        String str2 = "random";
        String str3 = "text";

        String result1 = getString(str1);
        String result2 = getString(str2);
        String result3 = getString(str3);
        assertEquals(result1.length(), 1) ;
        assertEquals(result2.length(), 1);
        assertEquals(result3.length(), 1);
    }
}
